FROM golang:1.19-alpine3.16 AS test

RUN apk update && apk add --no-cache git

ENV USER=appuser
ENV UID=10001
ENV CGO_ENABLED=0

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN apk update && apk upgrade && apk add --no-cache ca-certificates
RUN update-ca-certificates

WORKDIR $GOPATH/src/kuber/
COPY . .

# Using go mod.
RUN go mod tidy
# Build the binary.
RUN /bin/sh test.sh


FROM golang:1.19-alpine3.16 AS builder

RUN apk update && apk add --no-cache git ca-certificates

ARG TARGETOS
ARG TARGETARCH
ARG VERSION=v0.0.0-alpha
ENV USER=appuser
ENV UID=10001
ENV CGO_ENABLED=0

RUN GRPC_HEALTH_PROBE_VERSION=v0.4.7 && \
    wget -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-${TARGETARCH} && \
    chmod +x /bin/grpc_health_probe

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN update-ca-certificates

WORKDIR $GOPATH/src/kuber/
COPY . .
COPY go.mod ./
COPY go.sum ./
RUN --mount=type=cache,target=/go/pkg/mod go mod tidy

# Using go mod.
# RUN go mod tidy
# Build the binary.
RUN GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -ldflags="-w -s" -o /go/bin/kuber
############################
# STEP 2 build a small image
############################
FROM scratch

COPY --from=tarampampam/curl:7.78.0 /bin/curl /bin/curl

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group

COPY --from=builder /go/bin/kuber /go/bin/kuber
COPY --from=builder /bin/grpc_health_probe /bin/grpc_health_probe
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

USER appuser:appuser

ENTRYPOINT ["/go/bin/kuber"]
CMD ["${VERSION}"]
