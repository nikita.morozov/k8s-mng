package interactors

import (
	"fmt"
	apiv1 "k8s.io/api/core/v1"
	"kuber-crud/generator"
	"kuber-crud/models"
	"kuber-crud/repository"
	"kuber-crud/repository/kuberRepo"
)

type PersistentVolumeInteractor interface {
	Apply(item models.GenerateDTO, group string) (*models.Node, error)
	HardRun(item *apiv1.PersistentVolume) (*models.Node, error)
	Get(name string) (*models.Node, error)
	Delete(name string) error
}

type persistentVolumeInteractor struct {
	repo      kuberRepo.PersistentVolumeRepo
	redisRepo repository.RedisRepo
	generator generator.Generator
}

func (d persistentVolumeInteractor) Apply(val models.GenerateDTO, group string) (*models.Node, error) {
	config, _ := d.Get(val.Name)
	if config != nil {
		c, err := models.NodeToString[apiv1.PersistentVolume](config.PersistentVolume)
		if err != nil {
			return nil, err
		}

		val.SetConfig(c)
		val = d.generator.AddCustomValue(val)
		val.SetConfig(c)
	} else {
		s, err := d.generator.Generate(val)
		if err != nil {
			return nil, err
		}
		val.SetConfig(s.Config)
	}

	dep, err := models.GetDataByType[apiv1.PersistentVolume](val.Config)
	if err != nil {
		return nil, err
	}

	if config != nil {
		return d.update(dep, group, true)
	} else {
		return d.create(dep, group, true)
	}
}

func (d persistentVolumeInteractor) HardRun(item *apiv1.PersistentVolume) (*models.Node, error) {
	item.SetResourceVersion("0")

	config, _ := d.Get(item.GetName())
	if config != nil {
		return d.update(item, "", false)
	} else {
		return d.create(item, "", false)
	}
}

func (d persistentVolumeInteractor) create(item *apiv1.PersistentVolume, group string, useRedis bool) (*models.Node, error) {
	res, err := d.repo.Create(item)
	if err != nil {
		return nil, err
	}
	node := models.NewPersistentVolumeNode(res)
	if useRedis {
		err = d.redisRepo.Create(&node, group)
		if err != nil {
			return nil, err
		}
	}

	return &node, err
}
func (d persistentVolumeInteractor) update(item *apiv1.PersistentVolume, group string, useRedis bool) (*models.Node, error) {
	fmt.Println("Updating PpersistentVolume...")
	res, err := d.repo.Update(item)
	if err != nil {
		return nil, err
	}

	node := models.NewPersistentVolumeNode(res)
	if useRedis {
		err = d.redisRepo.Update(&node, group)
		if err != nil {
			return nil, err
		}
	}
	fmt.Printf("Updated PpersistentVolume %q.\n", res.GetName())
	return &node, err
}

func (d persistentVolumeInteractor) Get(name string) (*models.Node, error) {
	res, err := d.repo.Get(name)
	if err != nil {
		return nil, err
	}
	node := models.NewPersistentVolumeNode(res)
	return &node, err
}

func (d persistentVolumeInteractor) Delete(name string) error {
	item, err := d.Get(name)
	if err != nil {
		return err
	}

	err = d.repo.Delete(name)
	if err != nil {
		return err
	}
	err = d.redisRepo.Remove("", item.Type, item.Tier)
	if err != nil {
		return err
	}

	return err
}

func NewPersistentVolumeInteractor(
	repo kuberRepo.PersistentVolumeRepo,
	redisRepo repository.RedisRepo,
	generator generator.Generator,
) PersistentVolumeInteractor {
	return &persistentVolumeInteractor{
		repo:      repo,
		redisRepo: redisRepo,
		generator: generator,
	}
}
