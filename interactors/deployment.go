package interactors

import (
	"fmt"
	v1 "k8s.io/api/apps/v1"
	"kuber-crud/generator"
	"kuber-crud/models"
	"kuber-crud/repository"
	"kuber-crud/repository/kuberRepo"
)

type DeploymentInteractor interface {
	Apply(val models.GenerateDTO, group string) (*models.Node, error)
	HardRun(item *v1.Deployment) (*models.Node, error)
	Get(name string) (*models.Node, error)
	Delete(name string) error
}

type deploymentInteractor struct {
	repo      kuberRepo.DeploymentRepo
	redisRepo repository.RedisRepo
	generator generator.Generator
}

func (d deploymentInteractor) Apply(val models.GenerateDTO, group string) (*models.Node, error) {
	config, _ := d.Get(val.Name)
	if config != nil {
		c, err := models.NodeToString[v1.Deployment](config.Deployment)
		if err != nil {
			return nil, err
		}

		val.SetConfig(c)
		val = d.generator.AddCustomValue(val)
		val.SetConfig(c)
	} else {
		s, err := d.generator.Generate(val)
		if err != nil {
			return nil, err
		}
		val.SetConfig(s.Config)
	}

	//val = tools.AddParams(val)

	dep, err := models.GetDataByType[v1.Deployment](val.Config)
	if err != nil {
		return nil, err
	}

	if config != nil {
		return d.update(dep, group, true)
	} else {
		return d.create(dep, group, true)
	}
}

func (d deploymentInteractor) HardRun(item *v1.Deployment) (*models.Node, error) {
	item.SetResourceVersion("0")

	config, _ := d.Get(item.GetName())
	if config != nil {
		return d.update(item, "", false)
	} else {
		return d.create(item, "", false)
	}
}

func (d deploymentInteractor) create(item *v1.Deployment, group string, useRedis bool) (*models.Node, error) {
	res, err := d.repo.Create(item)
	if err != nil {
		return nil, err
	}

	node := models.NewDeploymentNode(res)
	if useRedis {
		err = d.redisRepo.Create(&node, group)
		if err != nil {
			return nil, err
		}
	}
	return &node, err
}
func (d deploymentInteractor) update(item *v1.Deployment, group string, useRedis bool) (*models.Node, error) {
	fmt.Println("Updating deployment...")
	res, err := d.repo.Update(item)
	if err != nil {
		return nil, err
	}
	node := models.NewDeploymentNode(res)

	if useRedis {
		err = d.redisRepo.Update(&node, group)
		if err != nil {
			return nil, err
		}
		fmt.Printf("Updated deployment %q.\n", res.GetName())
	}

	return &node, err
}

func (d deploymentInteractor) Get(name string) (*models.Node, error) {
	res, err := d.repo.Get(name)
	if err != nil {
		return nil, err
	}

	node := models.NewDeploymentNode(res)
	return &node, err
}

func (d deploymentInteractor) Delete(name string) error {
	_, err := d.Get(name)
	if err != nil {
		return err
	}

	return d.repo.Delete(name)
}

func NewDeploymentInteractor(
	repo kuberRepo.DeploymentRepo,
	redisRepo repository.RedisRepo,
	generator generator.Generator,
) DeploymentInteractor {
	return &deploymentInteractor{
		repo:      repo,
		redisRepo: redisRepo,
		generator: generator,
	}
}
