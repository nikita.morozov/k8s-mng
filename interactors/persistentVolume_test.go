package interactors

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	apiv1 "k8s.io/api/core/v1"
	"kuber-crud/generator"
	mocks2 "kuber-crud/generator/mocks"
	"kuber-crud/models"
	"kuber-crud/repository/mocks"
	"strings"
	"testing"
)

type PersistentVolumeInteractorSuite struct {
	suite.Suite

	redisRepo      *mocks.RedisRepo
	repo           *mocks.PersistentVolumeRepo
	generate       *mocks2.Generator
	interactor     PersistentVolumeInteractor
	localGenerator generator.Generator
	deployment     map[string]any
	group          string
	callbackURL    string
	config         string
}

func (s *PersistentVolumeInteractorSuite) BeforeTest(_, _ string) {
	repo := new(mocks.PersistentVolumeRepo)
	redisRepo := new(mocks.RedisRepo)
	generate := new(mocks2.Generator)
	s.redisRepo = redisRepo
	s.localGenerator = generator.NewGenerator("txt", "CALLBACK_NAME", "PARAMS_NAME")
	s.repo = repo
	s.generate = generate
	s.interactor = NewPersistentVolumeInteractor(repo, redisRepo, generate)
	s.group = "test-group"
	s.deployment = map[string]any{
		"kind":                               "PersistentVolumeClaim",
		"$name":                              "transservice",
		"selectorName":                       "testSelector",
		"spec.capacity.storage":              "1Gi",
		"spec.accessModes.0":                 "ReadWriteOnce",
		"spec.persistentVolumeReclaimPolicy": "Retain",
		"spec.storageClassName":              "fast",
		"spec.gcePersistentDisk.pdName":      "pd-ssd-disk-1",
	}
	s.callbackURL = "callbackURL"

	s.config = `{
 "kind": "PersistentVolume",
 "apiVersion": "v1",
 "metadata": {
   "name": "test",
   "labels": {
     "app": "crypto-fairy"
   }
 },
 "spec": {
   "capacity": {
     "storage": "2Gi"
   },
   "accessModes": [
     "ReadWriteOnce"
   ],
   "persistentVolumeReclaimPolicy": "Retain",
   "storageClassName": "fast",
   "gcePersistentDisk": {
     "pdName": "1"
   }
 }
}`

}

func (s *PersistentVolumeInteractorSuite) localGeneratePersistentVolumeModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.deployment)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

func (s *PersistentVolumeInteractorSuite) getConfig() (*apiv1.PersistentVolume, error) {
	var item *apiv1.PersistentVolume
	err := json.Unmarshal([]byte(s.config), &item)
	return item, err
}

func TestPersistentVolumeInteractorSuite(t *testing.T) {
	suite.Run(t, new(PersistentVolumeInteractorSuite))
}

// Apply new config
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeApplyNewConfig() {
	preConfig := s.localGeneratePersistentVolumeModel()
	resultConfig := s.localGeneratePersistentVolumeModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.PersistentVolume](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewPersistentVolumeNode(dep)
	s.repo.On("Create", node.PersistentVolume).Return(node.PersistentVolume, nil)
	s.redisRepo.On("Create", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.PersistentVolume, dep)
}

// Update new config
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeApplyUpdate() {
	preConfig := s.localGeneratePersistentVolumeModel()
	resultConfig := s.localGeneratePersistentVolumeModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.PersistentVolume](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewPersistentVolumeNode(dep)
	s.repo.On("Update", node.PersistentVolume).Return(node.PersistentVolume, nil)
	s.redisRepo.On("Update", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.PersistentVolume, dep)
}

// Update new config with error
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeApplyUpdateWithError() {
	preConfig := s.localGeneratePersistentVolumeModel()
	resultConfig := s.localGeneratePersistentVolumeModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.PersistentVolume](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewPersistentVolumeNode(dep)

	s.repo.On("Update", node.PersistentVolume).Return(nil, errors.New("errors.kuber"))
	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Apply new config
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeApplyWithError() {
	preConfig := s.localGeneratePersistentVolumeModel()
	resultConfig := s.localGeneratePersistentVolumeModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.PersistentVolume](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewPersistentVolumeNode(dep)
	s.repo.On("Create", node.PersistentVolume).Return(nil, errors.New("errros.kuber"))

	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Get
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)

	item, err := s.interactor.Get(config.Name)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.IsType(s.T(), item, &models.Node{})
}

// Method Get with error
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeGetWithError() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found"))

	item, err := s.interactor.Get(config.Name)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Delete
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeDelete() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(nil)
	node := models.NewPersistentVolumeNode(config)
	s.redisRepo.On("Remove", "", node.Type, node.Tier).Return(nil)
	err = s.interactor.Delete(config.Name)
	require.NoError(s.T(), err)
}

// Method Delete with error kuber
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeDeleteWithErrorInKuber() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(errors.New("not.found.servie"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}

// Method Delete with error get
func (s *PersistentVolumeInteractorSuite) Test_PersistentVolumeDeleteWithErrorInGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found.config"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}
