package interactors

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	apiv1 "k8s.io/api/core/v1"
	"kuber-crud/generator"
	mocks2 "kuber-crud/generator/mocks"
	"kuber-crud/models"
	"kuber-crud/repository/mocks"
	"strings"
	"testing"
)

type ConfigMapInteractorSuite struct {
	suite.Suite

	redisRepo      *mocks.RedisRepo
	repo           *mocks.ConfigMapRepo
	generate       *mocks2.Generator
	interactor     ConfigMapInteractor
	localGenerator generator.Generator
	deployment     map[string]any
	group          string
	callbackURL    string
	config         string
}

func (s *ConfigMapInteractorSuite) BeforeTest(_, _ string) {
	repo := new(mocks.ConfigMapRepo)
	redisRepo := new(mocks.RedisRepo)
	generate := new(mocks2.Generator)
	s.redisRepo = redisRepo
	s.localGenerator = generator.NewGenerator("txt", "CALLBACK_NAME", "PARAMS_NAME")
	s.repo = repo
	s.generate = generate
	s.interactor = NewConfigMapInteractor(repo, redisRepo, generate)
	s.group = "test-group"
	s.deployment = map[string]any{
		"kind":             "configmap",
		"$name":            "transservice",
		"selectorName":     "testSelector",
		"data.SOCKET_URL1": "SOCKET_URL1",
	}
	s.callbackURL = "callbackURL"

	s.config = `{
 "apiVersion": "v1",
 "data": {
   "SOCKET_URL": "http://test:8080",
   "DOMAIN": "http://test"
 },
 "kind": "ConfigMap",
 "metadata": {
   "creationTimestamp": null,
   "labels": {
     "io.kompose.service": "test-env"
   },
   "name": "test-env"
 }
}`

}

func (s *ConfigMapInteractorSuite) localGenerateConfigMapModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.deployment)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

func (s *ConfigMapInteractorSuite) getConfig() (*apiv1.ConfigMap, error) {
	var item *apiv1.ConfigMap
	err := json.Unmarshal([]byte(s.config), &item)
	return item, err
}

func TestConfigMapInteractorSuite(t *testing.T) {
	suite.Run(t, new(ConfigMapInteractorSuite))
}

// Apply new config
func (s *ConfigMapInteractorSuite) Test_ConfigMapApplyNewConfig() {
	preConfig := s.localGenerateConfigMapModel()
	resultConfig := s.localGenerateConfigMapModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.ConfigMap](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewConfigMapNode(dep)
	s.repo.On("Create", node.ConfigMap).Return(node.ConfigMap, nil)
	s.redisRepo.On("Create", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.ConfigMap, dep)
}

// Update new config
func (s *ConfigMapInteractorSuite) Test_ConfigMapApplyUpdate() {
	preConfig := s.localGenerateConfigMapModel()
	resultConfig := s.localGenerateConfigMapModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.ConfigMap](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewConfigMapNode(dep)
	s.repo.On("Update", node.ConfigMap).Return(node.ConfigMap, nil)
	s.redisRepo.On("Update", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.ConfigMap, dep)
}

// Update new config with error
func (s *ConfigMapInteractorSuite) Test_ConfigMapApplyUpdateWithError() {
	preConfig := s.localGenerateConfigMapModel()
	resultConfig := s.localGenerateConfigMapModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.ConfigMap](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewConfigMapNode(dep)

	s.repo.On("Update", node.ConfigMap).Return(nil, errors.New("errors.kuber"))
	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Apply new config
func (s *ConfigMapInteractorSuite) Test_ConfigMapApplyWithError() {
	preConfig := s.localGenerateConfigMapModel()
	resultConfig := s.localGenerateConfigMapModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.ConfigMap](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewConfigMapNode(dep)
	s.repo.On("Create", node.ConfigMap).Return(nil, errors.New("errros.kuber"))

	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Get
func (s *ConfigMapInteractorSuite) Test_ConfigMapGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)

	item, err := s.interactor.Get(config.Name)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.IsType(s.T(), item, &models.Node{})
}

// Method Get with error
func (s *ConfigMapInteractorSuite) Test_ConfigMapGetWithError() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found"))

	item, err := s.interactor.Get(config.Name)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Delete
func (s *ConfigMapInteractorSuite) Test_ConfigMapDelete() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(nil)
	node := models.NewConfigMapNode(config)
	s.redisRepo.On("Remove", "", node.Type, node.Tier).Return(nil)
	err = s.interactor.Delete(config.Name)
	require.NoError(s.T(), err)
}

// Method Delete with error kuber
func (s *ConfigMapInteractorSuite) Test_ConfigMapDeleteWithErrorInKuber() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(errors.New("not.found.servie"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}

// Method Delete with error get
func (s *ConfigMapInteractorSuite) Test_ConfigMapDeleteWithErrorInGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found.config"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}
