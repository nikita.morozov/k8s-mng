package interactors

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	apiv1 "k8s.io/api/core/v1"
	"kuber-crud/generator"
	mocks2 "kuber-crud/generator/mocks"
	"kuber-crud/models"
	"kuber-crud/repository/mocks"
	"strings"
	"testing"
)

type ServiceInteractorSuite struct {
	suite.Suite

	redisRepo      *mocks.RedisRepo
	repo           *mocks.ServiceRepo
	generate       *mocks2.Generator
	interactor     ServiceInteractor
	localGenerator generator.Generator
	deployment     map[string]any
	group          string
	callbackURL    string
	config         string
}

func (s *ServiceInteractorSuite) BeforeTest(_, _ string) {
	repo := new(mocks.ServiceRepo)
	redisRepo := new(mocks.RedisRepo)
	generate := new(mocks2.Generator)
	s.redisRepo = redisRepo
	s.localGenerator = generator.NewGenerator("txt", "CALLBACK_NAME", "PARAMS_NAME")
	s.repo = repo
	s.generate = generate
	s.interactor = NewServiceInteractor(repo, redisRepo, generate)
	s.group = "test-group"
	s.deployment = map[string]any{
		"kind":                    "service",
		"$name":                   "translate",
		"selectorName":            "testSelector",
		"spec.type":               "NodePort",
		"spec.ports.0.name":       "trans-grpc",
		"spec.ports.0.port":       2021,
		"spec.ports.0.protocol":   "TCP",
		"spec.ports.0.targetPort": 3001,
		"spec.ports.1.name":       "trans-http",
		"spec.ports.1.port":       2020,
		"spec.ports.1.targetPort": 3000,

		"params": map[string]any{
			"param1": "value1",
			"param2": "value2",
		},
	}
	s.callbackURL = "callbackURL"
	s.config = `{
  "apiVersion": "v1",
  "kind": "Service",
  "metadata": {
    "name": "translate",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "type": "NodePort",
    "selector": {
      "app": "crypto-fairy",
      "tier": "TransService"
    },
    "ports": [
      {
        "name": "trans-grpc",
        "port": 2021,
        "protocol": "TCP",
        "targetPort": 3001
      },
      {
        "name": "trans-http",
        "port": 2020,
        "targetPort": 3000
      }
    ]
  }
}`

}

func (s *ServiceInteractorSuite) localGenerateServiceModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.deployment)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

func (s *ServiceInteractorSuite) getConfig() (*apiv1.Service, error) {
	var item *apiv1.Service
	err := json.Unmarshal([]byte(s.config), &item)
	return item, err
}

func TestServiceInteractorSuite(t *testing.T) {
	suite.Run(t, new(ServiceInteractorSuite))
}

// Apply new config
func (s *ServiceInteractorSuite) Test_ServiceApplyNewConfig() {
	preConfig := s.localGenerateServiceModel()
	resultConfig := s.localGenerateServiceModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.Service](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewServiceNode(dep)
	s.repo.On("Create", node.Service).Return(node.Service, nil)
	s.redisRepo.On("Create", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.Service, dep)
}

// Update new config
func (s *ServiceInteractorSuite) Test_ServiceApplyUpdate() {
	preConfig := s.localGenerateServiceModel()
	resultConfig := s.localGenerateServiceModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.Service](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewServiceNode(dep)
	s.repo.On("Update", node.Service).Return(node.Service, nil)
	s.redisRepo.On("Update", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.Service, dep)
}

// Update new config with error
func (s *ServiceInteractorSuite) Test_ServiceApplyUpdateWithError() {
	preConfig := s.localGenerateServiceModel()
	resultConfig := s.localGenerateServiceModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.Service](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewServiceNode(dep)

	s.repo.On("Update", node.Service).Return(nil, errors.New("errors.kuber"))
	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Apply new config
func (s *ServiceInteractorSuite) Test_ServiceApplyWithError() {
	preConfig := s.localGenerateServiceModel()
	resultConfig := s.localGenerateServiceModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.Service](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewServiceNode(dep)
	s.repo.On("Create", node.Service).Return(nil, errors.New("errros.kuber"))

	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Get
func (s *ServiceInteractorSuite) Test_ServiceGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)

	item, err := s.interactor.Get(config.Name)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.IsType(s.T(), item, &models.Node{})
}

// Method Get with error
func (s *ServiceInteractorSuite) Test_ServiceGetWithError() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found"))

	item, err := s.interactor.Get(config.Name)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Delete
func (s *ServiceInteractorSuite) Test_ServiceDelete() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(nil)
	node := models.NewServiceNode(config)
	s.redisRepo.On("Remove", "", node.Type, node.Tier).Return(nil)
	err = s.interactor.Delete(config.Name)
	require.NoError(s.T(), err)
}

// Method Delete with error kuber
func (s *ServiceInteractorSuite) Test_ServiceDeleteWithErrorInKuber() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(errors.New("not.found.servie"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}

// Method Delete with error get
func (s *ServiceInteractorSuite) Test_ServiceDeleteWithErrorInGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found.config"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}
