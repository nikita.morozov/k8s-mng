package interactors

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	apiv1 "k8s.io/api/core/v1"
	"kuber-crud/generator"
	mocks2 "kuber-crud/generator/mocks"
	"kuber-crud/models"
	"kuber-crud/repository/mocks"
	"strings"
	"testing"
)

type PersistentVolumeClaimInteractorSuite struct {
	suite.Suite

	redisRepo      *mocks.RedisRepo
	repo           *mocks.PersistentVolumeClaimRepo
	generate       *mocks2.Generator
	interactor     PersistentVolumeClaimInteractor
	localGenerator generator.Generator
	deployment     map[string]any
	group          string
	callbackURL    string
	config         string
}

func (s *PersistentVolumeClaimInteractorSuite) BeforeTest(_, _ string) {
	repo := new(mocks.PersistentVolumeClaimRepo)
	redisRepo := new(mocks.RedisRepo)
	generate := new(mocks2.Generator)
	s.redisRepo = redisRepo
	s.localGenerator = generator.NewGenerator("txt", "CALLBACK_NAME", "PARAMS_NAME")
	s.repo = repo
	s.generate = generate
	s.interactor = NewPersistentVolumeClaimInteractor(repo, redisRepo, generate)
	s.group = "test-group"
	s.deployment = map[string]any{
		"kind":                               "PersistentVolume",
		"$name":                              "transservice",
		"selectorName":                       "testSelector",
		"spec.capacity.storage":              "1Gi",
		"spec.accessModes.0":                 "ReadWriteOnce",
		"spec.persistentVolumeReclaimPolicy": "Retain",
		"spec.storageClassName":              "fast",
		"spec.gcePersistentDisk.pdName":      "pd-ssd-disk-1",
	}
	s.callbackURL = "callbackURL"

	s.config = `{
 "apiVersion": "v1",
 "kind": "PersistentVolumeClaim",
 "metadata": {
   "name": "test",
   "labels": {
     "app": "crypto-fairy"
   }
 },
 "spec": {
   "storageClassName": "hcloud-volumes",
   "accessModes": [
     "ReadWriteOnce"
   ],
   "resources": {
     "requests": {
       "storage": "50Gi"
     }
   }
 }
}`

}

func (s *PersistentVolumeClaimInteractorSuite) localGeneratePersistentVolumeClaimModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.deployment)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

func (s *PersistentVolumeClaimInteractorSuite) getConfig() (*apiv1.PersistentVolumeClaim, error) {
	var item *apiv1.PersistentVolumeClaim
	err := json.Unmarshal([]byte(s.config), &item)
	return item, err
}

func TestPersistentVolumeClaimInteractorSuite(t *testing.T) {
	suite.Run(t, new(PersistentVolumeClaimInteractorSuite))
}

// Apply new config
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimApplyNewConfig() {
	preConfig := s.localGeneratePersistentVolumeClaimModel()
	resultConfig := s.localGeneratePersistentVolumeClaimModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.PersistentVolumeClaim](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewPersistentVolumeClaimNode(dep)
	s.repo.On("Create", node.PersistentVolumeClaim).Return(node.PersistentVolumeClaim, nil)
	s.redisRepo.On("Create", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.PersistentVolumeClaim, dep)
}

// Update new config
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimApplyUpdate() {
	preConfig := s.localGeneratePersistentVolumeClaimModel()
	resultConfig := s.localGeneratePersistentVolumeClaimModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.PersistentVolumeClaim](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewPersistentVolumeClaimNode(dep)
	s.repo.On("Update", node.PersistentVolumeClaim).Return(node.PersistentVolumeClaim, nil)
	s.redisRepo.On("Update", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.PersistentVolumeClaim, dep)
}

// Update new config with error
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimApplyUpdateWithError() {
	preConfig := s.localGeneratePersistentVolumeClaimModel()
	resultConfig := s.localGeneratePersistentVolumeClaimModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[apiv1.PersistentVolumeClaim](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewPersistentVolumeClaimNode(dep)

	s.repo.On("Update", node.PersistentVolumeClaim).Return(nil, errors.New("errors.kuber"))
	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Apply new config
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimApplyWithError() {
	preConfig := s.localGeneratePersistentVolumeClaimModel()
	resultConfig := s.localGeneratePersistentVolumeClaimModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[apiv1.PersistentVolumeClaim](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewPersistentVolumeClaimNode(dep)
	s.repo.On("Create", node.PersistentVolumeClaim).Return(nil, errors.New("errros.kuber"))

	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Get
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)

	item, err := s.interactor.Get(config.Name)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.IsType(s.T(), item, &models.Node{})
}

// Method Get with error
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimGetWithError() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found"))

	item, err := s.interactor.Get(config.Name)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Delete
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimDelete() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(nil)
	node := models.NewPersistentVolumeClaimNode(config)
	s.redisRepo.On("Remove", "", node.Type, node.Tier).Return(nil)
	err = s.interactor.Delete(config.Name)
	require.NoError(s.T(), err)
}

// Method Delete with error kuber
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimDeleteWithErrorInKuber() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(errors.New("not.found.servie"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}

// Method Delete with error get
func (s *PersistentVolumeClaimInteractorSuite) Test_PersistentVolumeClaimDeleteWithErrorInGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found.config"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}
