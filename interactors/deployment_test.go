package interactors

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	v1 "k8s.io/api/apps/v1"
	"kuber-crud/generator"
	mocks2 "kuber-crud/generator/mocks"
	"kuber-crud/models"
	"kuber-crud/repository/mocks"
	"strings"
	"testing"
)

type DeploymentInteractorSuite struct {
	suite.Suite

	redisRepo      *mocks.RedisRepo
	repo           *mocks.DeploymentRepo
	generate       *mocks2.Generator
	interactor     DeploymentInteractor
	localGenerator generator.Generator
	deployment     map[string]any
	group          string
	callbackURL    string
	config         string
}

func (s *DeploymentInteractorSuite) BeforeTest(_, _ string) {
	repo := new(mocks.DeploymentRepo)
	redisRepo := new(mocks.RedisRepo)
	generate := new(mocks2.Generator)
	s.redisRepo = redisRepo
	s.localGenerator = generator.NewGenerator("txt", "CALLBACK_NAME", "PARAMS_NAME")
	s.repo = repo
	s.generate = generate
	s.interactor = NewDeploymentInteractor(repo, redisRepo, generate)
	s.group = "test-group"
	s.deployment = map[string]any{
		"kind":         "deployment",
		"$name":        "transservice",
		"selectorName": "testSelector",
		"env": map[string]any{
			"FieldENV3": "ValueFieldEnv",
		},
		//"spec.template.spec.containers.0.image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.14",
		"image": "imageTest",
		"spec.template.spec.containers.0.ports.0.name":          "trans-grpc",
		"spec.template.spec.containers.0.ports.0.containerPort": 3001,
		"spec.template.spec.containers.0.ports.1.name":          "trans-http",
		"spec.template.spec.containers.0.ports.1.containerPort": 3000,
		//"spec.template.spec.containers.0.env.0.name":  "TEST_FFF",
		//"spec.template.spec.containers.0.env.0.value": "TEST_111",
		"spec.template.spec.containers.0.envFrom.0.secretRef.name": "db-credentials",
		"spec.template.spec.restartPolicy":                         "Always",
		"spec.template.spec.imagePullSecrets.0.name":               "gitlab-pull-secret",

		"params": map[string]any{
			"param1": "value1",
			"param2": "value2",
		},
	}
	s.callbackURL = "callbackURL"
	s.config = `{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "transservice",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "selector": {
      "matchLabels": {
        "app": "crypto-fairy"
      }
    },
    "template": {
      "metadata": {
        "labels": {
          "app": "crypto-fairy",
          "tier": "TransService"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "transservice",
            "image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.15",
            "ports": [
              {
                "name": "trans-grpc",
                "containerPort": 3001
              },
              {
                "name": "trans-http",
                "containerPort": 3000
              }
            ],
            "env": [
              {
                "name": "HTTP_SERVER",
                "value": "0.0.0.0:3000"
              },
              {
                "name": "GRPC_SERVER",
                "value": "0.0.0.0:3001"
              },
              {
                "name": "AUTO_TRANSLATE",
                "value": "FALSE"
              }
            ],
            "envFrom": [
              {
                "secretRef": {
                  "name": "db-credentials"
                }
              }
            ]
          }
        ],
        "restartPolicy": "Always",
        "imagePullSecrets": [
          {
            "name": "gitlab-pull-secret"
          }
        ]
      }
    }
  }
}`

}

func (s *DeploymentInteractorSuite) localGenerateDeploymentModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.deployment)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

func (s *DeploymentInteractorSuite) getConfig() (*v1.Deployment, error) {
	var item *v1.Deployment
	err := json.Unmarshal([]byte(s.config), &item)
	return item, err
}

func TestDeploymentInteractorSuite(t *testing.T) {
	suite.Run(t, new(DeploymentInteractorSuite))
}

// Apply new config
func (s *DeploymentInteractorSuite) Test_DeploymentApplyNewConfig() {
	preConfig := s.localGenerateDeploymentModel()
	resultConfig := s.localGenerateDeploymentModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[v1.Deployment](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewDeploymentNode(dep)
	s.repo.On("Create", node.Deployment).Return(node.Deployment, nil)
	s.redisRepo.On("Create", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.Deployment, dep)
}

// Update new config
func (s *DeploymentInteractorSuite) Test_DeploymentApplyUpdate() {
	preConfig := s.localGenerateDeploymentModel()
	resultConfig := s.localGenerateDeploymentModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[v1.Deployment](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewDeploymentNode(dep)
	s.repo.On("Update", node.Deployment).Return(node.Deployment, nil)
	s.redisRepo.On("Update", &node, s.group).Return(nil)

	item, err := s.interactor.Apply(preConfig, s.group)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.Equal(s.T(), item.Deployment, dep)
}

// Update new config with error
func (s *DeploymentInteractorSuite) Test_DeploymentApplyUpdateWithError() {
	preConfig := s.localGenerateDeploymentModel()
	resultConfig := s.localGenerateDeploymentModel()

	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	dep, err := models.GetDataByType[v1.Deployment](resultConfig.Config)
	require.NoError(s.T(), err)

	valResulktCOnfig := s.localGenerator.AddCustomValue(resultConfig)
	resultConfig.SetConfig(valResulktCOnfig.Config)

	s.repo.On("Get", preConfig.Name).Return(dep, nil)
	s.generate.On("AddCustomValue", mock.Anything).Return(preConfig)

	node := models.NewDeploymentNode(dep)

	s.repo.On("Update", node.Deployment).Return(nil, errors.New("errors.kuber"))
	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Apply new config
func (s *DeploymentInteractorSuite) Test_DeploymentApplyWithError() {
	preConfig := s.localGenerateDeploymentModel()
	resultConfig := s.localGenerateDeploymentModel()

	s.repo.On("Get", preConfig.Name).Return(nil, errors.New("not-found"))
	resultConfig, err := s.localGenerator.Generate(resultConfig)
	require.NoError(s.T(), err)
	s.generate.On("Generate", preConfig).Return(resultConfig, nil)

	dep, err := models.GetDataByType[v1.Deployment](resultConfig.Config)
	require.NoError(s.T(), err)

	node := models.NewDeploymentNode(dep)
	s.repo.On("Create", node.Deployment).Return(nil, errors.New("errros.kuber"))

	item, err := s.interactor.Apply(preConfig, s.group)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Get
func (s *DeploymentInteractorSuite) Test_DeploymentGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)

	item, err := s.interactor.Get(config.Name)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
	require.IsType(s.T(), item, &models.Node{})
}

// Method Get with error
func (s *DeploymentInteractorSuite) Test_DeploymentGetWithError() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found"))

	item, err := s.interactor.Get(config.Name)
	require.Error(s.T(), err)
	require.Nil(s.T(), item)
}

// Method Delete
func (s *DeploymentInteractorSuite) Test_DeploymentDelete() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(nil)
	node := models.NewDeploymentNode(config)
	s.redisRepo.On("Remove", "", node.Type, node.Tier).Return(nil)
	err = s.interactor.Delete(config.Name)
	require.NoError(s.T(), err)
}

// Method Delete with error kuber
func (s *DeploymentInteractorSuite) Test_DeploymentDeleteWithErrorInKuber() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(config, nil)
	s.repo.On("Delete", config.Name).Return(errors.New("not.found.servie"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}

// Method Delete with error get
func (s *DeploymentInteractorSuite) Test_DeploymentDeleteWithErrorInGet() {
	config, err := s.getConfig()
	require.NoError(s.T(), err)

	s.repo.On("Get", config.Name).Return(nil, errors.New("not.found.config"))
	err = s.interactor.Delete(config.Name)
	require.Error(s.T(), err)
}
