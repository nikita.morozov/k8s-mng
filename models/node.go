package models

import (
	"encoding/json"
	v1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
)

const (
	DEPLOYMENT              = "deployment"
	SERVICE                 = "service"
	CONFIGMAP               = "configmap"
	PERSISTENT_VOLUME       = "persistentvolume"
	PERSISTENT_VOLUME_CLAIM = "persistentvolumeclaim"
)

type Node struct {
	Tier                  string                       `json:"tier"`
	Type                  string                       `json:"type"`
	Deployment            *v1.Deployment               `json:"deployment"`
	Service               *apiv1.Service               `json:"service"`
	ConfigMap             *apiv1.ConfigMap             `json:"configMap"`
	PersistentVolume      *apiv1.PersistentVolume      `json:"persistentVolume"`
	PersistentVolumeClaim *apiv1.PersistentVolumeClaim `json:"persistentVolumeClaim"`
}

func NewDeploymentNode(deployment *v1.Deployment) Node {
	tier := GetTierDeployment(deployment)
	node := Node{
		//Id:   string(deployment.GetUID()),
		Tier:       tier,
		Type:       DEPLOYMENT,
		Deployment: deployment,
	}

	return node
}

func NewServiceNode(service *apiv1.Service) Node {
	tier := GetTierInService(service)
	node := Node{
		//Id:   string(deployment.GetUID()),
		Tier:    tier,
		Type:    SERVICE,
		Service: service,
	}

	return node
}

func NewConfigMapNode(configMap *apiv1.ConfigMap) Node {
	node := Node{
		Tier:      configMap.GetName(),
		Type:      CONFIGMAP,
		ConfigMap: configMap,
	}

	return node
}

func NewPersistentVolumeNode(persistentVolume *apiv1.PersistentVolume) Node {
	node := Node{
		Tier:             persistentVolume.GetName(),
		Type:             PERSISTENT_VOLUME,
		PersistentVolume: persistentVolume,
	}

	return node
}

func NewPersistentVolumeClaimNode(persistentVolumeClaim *apiv1.PersistentVolumeClaim) Node {
	node := Node{
		Tier:                  persistentVolumeClaim.GetName(),
		Type:                  PERSISTENT_VOLUME_CLAIM,
		PersistentVolumeClaim: persistentVolumeClaim,
	}

	return node
}

func GetDataByType[T any](deployment string) (*T, error) {
	var val T
	err := json.Unmarshal([]byte(deployment), &val)
	if err != nil {
		return nil, err
	}
	return &val, err
}

func GetTierInService(item *apiv1.Service) string {
	values := item.Spec.Selector
	if values["Tier"] != "" {
		return values["Tier"]
	}
	return values["tier"]
}

func GetTierDeployment(item *v1.Deployment) string {
	values := item.Spec.Template.Labels
	if values["Tier"] != "" {
		return values["Tier"]
	}
	return values["tier"]
}

func NodeToString[T any](node *T) (string, error) {
	bytes, err := json.Marshal(node)
	if err != nil {
		return "", err
	}
	return string(bytes), err
}
