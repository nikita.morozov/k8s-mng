package models

import "github.com/tidwall/gjson"

type GenerateDTO struct {
	Kind         string         `json:"kind"`
	Name         string         `json:"name"`
	SelectorName string         `json:"selectorName"`
	Meta         map[string]any `json:"meta"`
	CallbackURL  string         `json:"callbackUrl"`
	Config       string         `json:"config"`

	Image  string                  `json:"image"`
	Params map[string]any          `json:"params"`
	Env    map[string]gjson.Result `json:"env"`
}

func (g *GenerateDTO) SetConfig(config string) {
	g.Config = config
}
