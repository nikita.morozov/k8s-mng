package models

type RequestConfigs struct {
	Group  string           `json:"group"`
	Method string           `json:"method"`
	Items  []map[string]any `json:"items"`
	Params map[string]any   `json:"params"`
}

func NewRequestConfig(method, group string) *RequestConfigs {
	return &RequestConfigs{
		Method: method,
		Group:  group,
	}
}
