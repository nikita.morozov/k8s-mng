package models

import (
	"github.com/stretchr/testify/assert"
	v1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"testing"
)

func TestNode_NewServiceNode(t *testing.T) {
	service := NewServiceNode(&apiv1.Service{})

	assert.NotNil(t, service)
	assert.Equal(t, service.Type, SERVICE)
}

func TestNode_NewDeploymentNode(t *testing.T) {
	deployment := NewDeploymentNode(&v1.Deployment{})

	assert.NotNil(t, deployment)
	assert.Equal(t, deployment.Type, DEPLOYMENT)
}

func TestNode_NewConfigMapNode(t *testing.T) {
	configmap := NewConfigMapNode(&apiv1.ConfigMap{})

	assert.NotNil(t, configmap)
	assert.Equal(t, configmap.Type, CONFIGMAP)
}

func TestNode_NewPersistentVolumeNode(t *testing.T) {
	persistentVolume := NewPersistentVolumeNode(&apiv1.PersistentVolume{})

	assert.NotNil(t, persistentVolume)
	assert.Equal(t, persistentVolume.Type, PERSISTENT_VOLUME)
}

func TestNode_NewPersistentVolumeClaimNode(t *testing.T) {
	persistentVolumeClaim := NewPersistentVolumeClaimNode(&apiv1.PersistentVolumeClaim{})

	assert.NotNil(t, persistentVolumeClaim)
	assert.Equal(t, persistentVolumeClaim.Type, PERSISTENT_VOLUME_CLAIM)
}
