v1.0.5
* Fixed errors

v1.0.4
* Moved params in global object

v1.0.3
* Send object with meta for create config in k8s
* Generate config
* Added wait for connect nats
* Added stop/start/restart methods
* Update test

v1.0.2
* Update versions of libs
* Fixed apply method

v1.0.1
* Added test redisRepo
* Added test kuber repo
* Added test interactors
* Added test usecase
* Added test models
* Fixed ci/cd for run test step
* Update docker file for test 

v1.0.0
* Init k8s-manager
