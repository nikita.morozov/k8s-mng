package usecase

import (
	"encoding/json"
	"errors"
	"github.com/tidwall/gjson"
	interactors2 "kuber-crud/interactors"
	models2 "kuber-crud/models"
	"kuber-crud/repository"
	"strings"
)

type KuberUsecase interface {
	Apply(items []map[string]any, params map[string]any, group, callbackUrl string) error
	DeleteGroup(group string, removeRedis bool) error
	Stop() error
	Run() error
	RestartAll() error
	//Start() error
}

type kuberUsecase struct {
	deployment              interactors2.DeploymentInteractor
	service                 interactors2.ServiceInteractor
	configMap               interactors2.ConfigMapInteractor
	presentationVolume      interactors2.PersistentVolumeInteractor
	presentationVolumeClaim interactors2.PersistentVolumeClaimInteractor
	redisRepo               repository.RedisRepo
}

func (k kuberUsecase) Apply(items []map[string]any, params map[string]any, group, callbackUrl string) error {
	var err error

	if callbackUrl == "" {
		callbackUrl = "1"
		//return errors.New("apply.callback-url.not-set")
	}

	for _, item := range items {
		bytes, err := json.Marshal(item)
		if err != nil {
			return err
		}

		kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
		val := models2.GenerateDTO{
			Kind:         kind,
			Meta:         item,
			CallbackURL:  callbackUrl,
			Name:         gjson.GetBytes(bytes, "name").String(),
			SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
			Params:       params,
			Env:          gjson.GetBytes(bytes, "env").Map(),
			Image:        gjson.GetBytes(bytes, "image").String(),
		}

		switch val.Kind {
		case models2.DEPLOYMENT:
			_, err = k.deployment.Apply(val, group)
			if err != nil {
				return err
			}
		case models2.SERVICE:
			_, err = k.service.Apply(val, group)
			if err != nil {
				return err
			}
		case models2.CONFIGMAP:
			_, err = k.configMap.Apply(val, group)
			if err != nil {
				return err
			}
		case models2.PERSISTENT_VOLUME:
			_, err = k.presentationVolume.Apply(val, group)
			if err != nil {
				return err
			}
		case models2.PERSISTENT_VOLUME_CLAIM:
			_, err = k.presentationVolumeClaim.Apply(val, group)
			if err != nil {
				return err
			}
		}
	}

	return err
}

func (k kuberUsecase) Stop() error {
	return k.DeleteGroup("*", false)
}

func (k kuberUsecase) RestartAll() error {
	err := k.Stop()
	if err != nil {
		return err
	}
	err = k.Run()
	return err
}

func (k kuberUsecase) Run() error {
	list, err := k.redisRepo.List("*", "*", "*")
	if err != nil {
		return errors.New("List failed")
	}

	for _, val := range *list {
		switch val.Type {
		case models2.DEPLOYMENT:
			k.deployment.HardRun(val.Deployment)
		case models2.SERVICE:
			k.service.HardRun(val.Service)
		case models2.CONFIGMAP:
			k.configMap.HardRun(val.ConfigMap)
		case models2.PERSISTENT_VOLUME:
			k.presentationVolume.HardRun(val.PersistentVolume)
		case models2.PERSISTENT_VOLUME_CLAIM:
			k.presentationVolumeClaim.HardRun(val.PersistentVolumeClaim)
		}
	}
	return err
}

func (k kuberUsecase) DeleteGroup(group string, removeRedis bool) error {
	list, err := k.redisRepo.List(group, "*", "*")
	if err != nil {
		return errors.New("Group not found")
	}
	for _, item := range *list {
		switch strings.ToLower(item.Type) {
		case models2.DEPLOYMENT:
			err = k.deployment.Delete(item.Deployment.GetName())
		case models2.SERVICE:
			err = k.service.Delete(item.Service.GetName())
		case models2.CONFIGMAP:
			err = k.configMap.Delete(item.ConfigMap.GetName())
		case models2.PERSISTENT_VOLUME:
			err = k.presentationVolume.Delete(item.PersistentVolume.GetName())
		case models2.PERSISTENT_VOLUME_CLAIM:
			err = k.presentationVolumeClaim.Delete(item.PersistentVolume.GetName())
		}

		if err != nil {
			continue
		}

		if removeRedis {
			err = k.redisRepo.Remove(group, item.Type, item.Tier)
		}
	}

	return err
}

func NewKuberUsecase(
	deployment interactors2.DeploymentInteractor,
	service interactors2.ServiceInteractor,
	configMap interactors2.ConfigMapInteractor,
	presentationVolume interactors2.PersistentVolumeInteractor,
	presentationVolumeClaim interactors2.PersistentVolumeClaimInteractor,
	redisRepo repository.RedisRepo,
) KuberUsecase {
	return &kuberUsecase{
		deployment:              deployment,
		service:                 service,
		configMap:               configMap,
		presentationVolume:      presentationVolume,
		presentationVolumeClaim: presentationVolumeClaim,
		redisRepo:               redisRepo,
	}
}
