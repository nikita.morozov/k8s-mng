package usecase

import (
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	v1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	interactorMock "kuber-crud/interactors/mocks"
	models2 "kuber-crud/models"
	"kuber-crud/repository/mocks"
	"strings"
	"testing"
)

type UsecaseSuite struct {
	suite.Suite

	redisRepo                 *mocks.RedisRepo
	deploymentInteractor      *interactorMock.DeploymentInteractor
	serviceInteractor         *interactorMock.ServiceInteractor
	configmapInteractor       *interactorMock.ConfigMapInteractor
	presVolumeInteractor      *interactorMock.PersistentVolumeInteractor
	presVolumeClaimInteractor *interactorMock.PersistentVolumeClaimInteractor
	uc                        KuberUsecase

	deployment      map[string]any
	service         map[string]any
	configmap       map[string]any
	persVolume      map[string]any
	persVolumeClaim map[string]any

	configPVC     string
	configDep     string
	configService string
	configPV      string
	configMap     string

	group  string
	method string
}

func (s *UsecaseSuite) BeforeTest(_, _ string) {
	redisRepo := new(mocks.RedisRepo)
	configmapInteractor := new(interactorMock.ConfigMapInteractor)
	deploymentInteractor := new(interactorMock.DeploymentInteractor)
	serviceInteractor := new(interactorMock.ServiceInteractor)
	presVolumeInteractor := new(interactorMock.PersistentVolumeInteractor)
	presVolumeClaimInteractor := new(interactorMock.PersistentVolumeClaimInteractor)

	s.redisRepo = redisRepo
	s.configmapInteractor = configmapInteractor
	s.deploymentInteractor = deploymentInteractor
	s.serviceInteractor = serviceInteractor
	s.presVolumeInteractor = presVolumeInteractor
	s.presVolumeClaimInteractor = presVolumeClaimInteractor
	s.uc = NewKuberUsecase(deploymentInteractor, serviceInteractor, configmapInteractor, presVolumeInteractor, presVolumeClaimInteractor, redisRepo)
	s.group = "test-group"
	s.method = "test-method"
	s.configPV = `{
 "kind": "PersistentVolume",
 "apiVersion": "v1",
 "metadata": {
   "name": "test",
   "labels": {
     "app": "crypto-fairy"
   }
 },
 "spec": {
   "capacity": {
     "storage": "2Gi"
   },
   "accessModes": [
     "ReadWriteOnce"
   ],
   "persistentVolumeReclaimPolicy": "Retain",
   "storageClassName": "fast",
   "gcePersistentDisk": {
     "pdName": "1"
   }
 }
}`
	s.configPVC = `{
 "apiVersion": "v1",
 "kind": "PersistentVolumeClaim",
 "metadata": {
   "name": "test",
   "labels": {
     "app": "crypto-fairy"
   }
 },
 "spec": {
   "storageClassName": "hcloud-volumes",
   "accessModes": [
     "ReadWriteOnce"
   ],
   "resources": {
     "requests": {
       "storage": "50Gi"
     }
   }
 }
}`
	s.configMap = `{
 "apiVersion": "v1",
 "data": {
   "SOCKET_URL": "http://test:8080",
   "DOMAIN": "http://test"
 },
 "kind": "ConfigMap",
 "metadata": {
   "creationTimestamp": null,
   "labels": {
     "io.kompose.service": "test-env"
   },
   "name": "test-env"
 }
}`
	s.configService = `{
  "apiVersion": "v1",
  "kind": "Service",
  "metadata": {
    "name": "translate",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "type": "NodePort",
    "selector": {
      "app": "crypto-fairy",
      "tier": "TransService"
    },
    "ports": [
      {
        "name": "trans-grpc",
        "port": 2021,
        "protocol": "TCP",
        "targetPort": 3001
      },
      {
        "name": "trans-http",
        "port": 2020,
        "targetPort": 3000
      }
    ]
  }
}`
	s.configDep = `{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "transservice",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "selector": {
      "matchLabels": {
        "app": "crypto-fairy"
      }
    },
    "template": {
      "metadata": {
        "labels": {
          "app": "crypto-fairy",
          "tier": "TransService"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "transservice",
            "image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.15",
            "ports": [
              {
                "name": "trans-grpc",
                "containerPort": 3001
              },
              {
                "name": "trans-http",
                "containerPort": 3000
              }
            ],
            "env": [
              {
                "name": "HTTP_SERVER",
                "value": "0.0.0.0:3000"
              },
              {
                "name": "GRPC_SERVER",
                "value": "0.0.0.0:3001"
              },
              {
                "name": "AUTO_TRANSLATE",
                "value": "FALSE"
              }
            ],
            "envFrom": [
              {
                "secretRef": {
                  "name": "db-credentials"
                }
              }
            ]
          }
        ],
        "restartPolicy": "Always",
        "imagePullSecrets": [
          {
            "name": "gitlab-pull-secret"
          }
        ]
      }
    }
  }
}`
	s.configmap = map[string]any{
		"kind":             "configmap",
		"name":             "transservice",
		"selectorName":     "testSelector",
		"data.SOCKET_URL1": "SOCKET_URL1",
	}
	s.deployment = map[string]any{
		"kind":         "deployment",
		"name":         "transservice",
		"selectorName": "testSelector",
		"env": map[string]any{
			"FieldENV3": "ValueFieldEnv",
		},
		//"spec.template.spec.containers.0.image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.14",
		"image": "imageTest",
		"spec.template.spec.containers.0.ports.0.name":          "trans-grpc",
		"spec.template.spec.containers.0.ports.0.containerPort": 3001,
		"spec.template.spec.containers.0.ports.1.name":          "trans-http",
		"spec.template.spec.containers.0.ports.1.containerPort": 3000,
		//"spec.template.spec.containers.0.env.0.name":  "TEST_FFF",
		//"spec.template.spec.containers.0.env.0.value": "TEST_111",
		"spec.template.spec.containers.0.envFrom.0.secretRef.name": "db-credentials",
		"spec.template.spec.restartPolicy":                         "Always",
		"spec.template.spec.imagePullSecrets.0.name":               "gitlab-pull-secret",

		"params": map[string]any{
			"param1": "value1",
			"param2": "value2",
		},
	}
	s.persVolumeClaim = map[string]any{
		"kind":                               "PersistentVolumeClaim",
		"name":                               "transservice",
		"selectorName":                       "testSelector",
		"spec.capacity.storage":              "1Gi",
		"spec.accessModes.0":                 "ReadWriteOnce",
		"spec.persistentVolumeReclaimPolicy": "Retain",
		"spec.storageClassName":              "fast",
		"spec.gcePersistentDisk.pdName":      "pd-ssd-disk-1",
	}
	s.persVolume = map[string]any{
		"kind":                               "PersistentVolume",
		"name":                               "transservice",
		"selectorName":                       "testSelector",
		"spec.capacity.storage":              "1Gi",
		"spec.accessModes.0":                 "ReadWriteOnce",
		"spec.persistentVolumeReclaimPolicy": "Retain",
		"spec.storageClassName":              "fast",
		"spec.gcePersistentDisk.pdName":      "pd-ssd-disk-1",
	}
	s.service = map[string]any{
		"kind":                    "service",
		"name":                    "translate",
		"selectorName":            "testSelector",
		"spec.type":               "NodePort",
		"spec.ports.0.name":       "trans-grpc",
		"spec.ports.0.port":       2021,
		"spec.ports.0.protocol":   "TCP",
		"spec.ports.0.targetPort": 3001,
		"spec.ports.1.name":       "trans-http",
		"spec.ports.1.port":       2020,
		"spec.ports.1.targetPort": 3000,

		"params": map[string]any{
			"param1": "value1",
			"param2": "value2",
		},
	}

}

func TestUsecaseSuite(t *testing.T) {
	suite.Run(t, new(UsecaseSuite))
}

func (s *UsecaseSuite) getService() (*apiv1.Service, error) {
	var item *apiv1.Service
	err := json.Unmarshal([]byte(s.configService), &item)
	return item, err
}

func (s *UsecaseSuite) getDeployment() (*v1.Deployment, error) {
	var item *v1.Deployment
	err := json.Unmarshal([]byte(s.configDep), &item)
	return item, err
}

func (s *UsecaseSuite) getConfigMap() (*apiv1.ConfigMap, error) {
	var item *apiv1.ConfigMap
	err := json.Unmarshal([]byte(s.configMap), &item)
	return item, err
}

func (s *UsecaseSuite) getPersistentVolumeClaim() (*apiv1.PersistentVolumeClaim, error) {
	var item *apiv1.PersistentVolumeClaim
	err := json.Unmarshal([]byte(s.configPVC), &item)
	return item, err
}

func (s *UsecaseSuite) getPersistentVolume() (*apiv1.PersistentVolume, error) {
	var item *apiv1.PersistentVolume
	err := json.Unmarshal([]byte(s.configPV), &item)
	return item, err
}

// Apply one deployment config
func (s *UsecaseSuite) Test_ucApplyDeployment() {
	bytes, err := json.Marshal(s.deployment)
	require.NoError(s.T(), err)
	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}
	items := []map[string]any{
		s.deployment,
	}

	s.deploymentInteractor.On("Apply", val, s.group).Return(nil, nil)

	err = s.uc.Apply(items, nil, s.group, "callbackURL")
	require.NoError(s.T(), err)
}

// Apply one service config
func (s *UsecaseSuite) Test_ucApplyService() {
	bytes, err := json.Marshal(s.service)
	require.NoError(s.T(), err)
	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.service,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}
	items := []map[string]any{
		s.service,
	}

	s.serviceInteractor.On("Apply", val, s.group).Return(nil, nil)

	err = s.uc.Apply(items, nil, s.group, "callbackURL")
	require.NoError(s.T(), err)
}

// Apply one configmap config
func (s *UsecaseSuite) Test_ucApplyConfigMap() {
	bytes, err := json.Marshal(s.configmap)
	require.NoError(s.T(), err)
	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.configmap,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}
	items := []map[string]any{
		s.configmap,
	}

	s.configmapInteractor.On("Apply", val, s.group).Return(nil, nil)

	err = s.uc.Apply(items, nil, s.group, "callbackURL")
	require.NoError(s.T(), err)
}

// Apply one presentationVolume config
func (s *UsecaseSuite) Test_ucApplyPresentationVolume() {
	bytes, err := json.Marshal(s.persVolume)
	require.NoError(s.T(), err)
	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.persVolume,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}
	items := []map[string]any{
		s.persVolume,
	}

	s.presVolumeInteractor.On("Apply", val, s.group).Return(nil, nil)

	err = s.uc.Apply(items, nil, s.group, "callbackURL")
	require.NoError(s.T(), err)
}

// Apply one presentationVolumeClaim config
func (s *UsecaseSuite) Test_ucApplyPresentationVolumeClaim() {
	bytes, err := json.Marshal(s.persVolumeClaim)
	require.NoError(s.T(), err)
	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.persVolumeClaim,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}
	items := []map[string]any{
		s.persVolumeClaim,
	}

	s.presVolumeClaimInteractor.On("Apply", val, s.group).Return(nil, nil)

	err = s.uc.Apply(items, nil, s.group, "callbackURL")
	require.NoError(s.T(), err)
}

// Apply array two elements config
func (s *UsecaseSuite) Test_ucApplyServiceAndDeployment() {
	bytes, err := json.Marshal(s.deployment)
	require.NoError(s.T(), err)
	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}

	require.NoError(s.T(), err)

	bytes, err = json.Marshal(s.service)
	require.NoError(s.T(), err)
	kind = strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	valService := models2.GenerateDTO{
		Kind:         kind,
		Meta:         s.service,
		CallbackURL:  "callbackURL",
		Name:         gjson.GetBytes(bytes, "name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Env:          gjson.GetBytes(bytes, "env").Map(),
		Image:        gjson.GetBytes(bytes, "image").String(),
	}
	items := []map[string]any{
		s.service,
		s.deployment,
	}

	s.deploymentInteractor.On("Apply", val, s.group).Return(nil, nil)
	s.serviceInteractor.On("Apply", valService, s.group).Return(nil, nil)

	err = s.uc.Apply(items, nil, s.group, "callbackURL")
	require.NoError(s.T(), err)
}

func (s *UsecaseSuite) Test_ucDeleteGroup() {
	service, err := s.getService()
	require.NoError(s.T(), err)
	require.NotNil(s.T(), service)
	deployment, err := s.getDeployment()
	require.NoError(s.T(), err)
	require.NotNil(s.T(), deployment)

	list := []models2.Node{
		models2.NewServiceNode(service),
		models2.NewDeploymentNode(deployment),
	}

	s.redisRepo.On("List", s.group, "*", "*").Return(&list, nil)
	s.serviceInteractor.On("Delete", service.Name).Return(nil)
	s.deploymentInteractor.On("Delete", deployment.Name).Return(nil)

	for _, i := range list {
		s.redisRepo.On("Remove", s.group, i.Type, i.Tier).Return(nil)
	}

	err = s.uc.DeleteGroup(s.group, true)
	require.NoError(s.T(), err)
}

// Delete group with err in redis list
func (s *UsecaseSuite) Test_ucDeleteGroupWithErrorInRedisList() {
	s.redisRepo.On("List", s.group, "*", "*").Return(nil, errors.New("list.not.found"))

	err := s.uc.DeleteGroup(s.group, true)
	require.Error(s.T(), err)
}

// Delete group with error in redis method remove
func (s *UsecaseSuite) Test_ucDeleteGroupWithErrorInRedisRemove() {
	service, err := s.getService()
	require.NoError(s.T(), err)
	require.NotNil(s.T(), service)
	deployment, err := s.getDeployment()
	require.NoError(s.T(), err)
	require.NotNil(s.T(), deployment)

	serviceNode := models2.NewServiceNode(service)
	deploymentNode := models2.NewDeploymentNode(deployment)

	list := []models2.Node{
		serviceNode,
		deploymentNode,
	}

	s.redisRepo.On("List", s.group, "*", "*").Return(&list, nil)
	s.serviceInteractor.On("Delete", service.Name).Return(nil)
	s.deploymentInteractor.On("Delete", deployment.Name).Return(nil)

	s.redisRepo.On("Remove", s.group, serviceNode.Type, serviceNode.Tier).Return(errors.New("remove.failed"))
	s.redisRepo.On("Remove", s.group, deploymentNode.Type, deploymentNode.Tier).Return(nil)

	err = s.uc.DeleteGroup(s.group, true)
	require.NoError(s.T(), err)
}

// Delete group with error in deployment method remove
func (s *UsecaseSuite) Test_ucDeleteGroupWithErrorInDeploymentDelete() {
	service, err := s.getService()
	require.NoError(s.T(), err)
	require.NotNil(s.T(), service)
	deployment, err := s.getDeployment()
	require.NoError(s.T(), err)
	require.NotNil(s.T(), deployment)

	serviceNode := models2.NewServiceNode(service)
	deploymentNode := models2.NewDeploymentNode(deployment)

	list := []models2.Node{
		serviceNode,
		deploymentNode,
	}

	s.redisRepo.On("List", s.group, "*", "*").Return(&list, nil)
	s.serviceInteractor.On("Delete", service.Name).Return(nil)
	s.deploymentInteractor.On("Delete", deployment.Name).Return(errors.New("deployment.failed"))

	s.redisRepo.On("Remove", s.group, serviceNode.Type, serviceNode.Tier).Return(nil)

	err = s.uc.DeleteGroup(s.group, true)
	require.Error(s.T(), err)
	require.Equal(s.T(), err.Error(), "deployment.failed")
}
