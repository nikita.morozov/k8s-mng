FROM golang:1.19-alpine3.16 AS builder

RUN apk update && apk add --no-cache git ca-certificates curl

ARG TARGETOS
ARG TARGETARCH
ARG VERSION=v0.0.0-alpha
ENV USER=appuser
ENV UID=10001
ENV CGO_ENABLED=0

RUN go install github.com/go-delve/delve/cmd/dlv@latest

RUN adduser \
    --disabled-password \
    --gecos "" \
    --home "/nonexistent" \
    --shell "/sbin/nologin" \
    --no-create-home \
    --uid "${UID}" \
    "${USER}"

RUN update-ca-certificates

WORKDIR $GOPATH/src/kuber
COPY . .
COPY go.mod ./
COPY go.sum ./
RUN --mount=type=cache,target=/go/pkg/mod go mod tidy

RUN --mount=target=. \
   --mount=type=cache,target=/go/pkg/mod/ \
   --mount=type=cache,target=/root/.cache/go-build/ GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -gcflags "all=-N -l" -o /go/bin/kuber

EXPOSE 3000
EXPOSE 3001
EXPOSE 40002

#ENTRYPOINT "/go/bin/kuber"
ENTRYPOINT ["/go/bin/dlv", "--continue", "--listen=:40002", "--log", "--headless=true", "--api-version=2", "--accept-multiclient", "exec", "/go/bin/kuber"]
