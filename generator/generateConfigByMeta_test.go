package generator

import (
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tidwall/gjson"
	"kuber-crud/models"
	"strings"
	"testing"
)

type GeneratorSuite struct {
	suite.Suite
	app          string
	callbackName string
	callbackURL  string
	paramsName   string
	generator    Generator
	deployment   map[string]any
	service      map[string]any
}

func (s *GeneratorSuite) BeforeTest(_, _ string) {
	s.app = "txt"
	s.callbackName = "TXT_CALLBACK_METHOD"
	s.paramsName = "TXT_SRV_PARAMS}"
	s.callbackURL = "test_url"
	s.generator = NewGenerator(s.app, s.callbackName, s.paramsName)
	s.deployment = map[string]any{
		"kind":         "deployment",
		"$name":        "transservice",
		"selectorName": "testSelector",
		"env": map[string]any{
			"FieldENV3": "ValueFieldEnv",
		},
		//"spec.template.spec.containers.0.image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.14",
		"image": "imageTest",
		"spec.template.spec.containers.0.ports.0.name":          "trans-grpc",
		"spec.template.spec.containers.0.ports.0.containerPort": 3001,
		"spec.template.spec.containers.0.ports.1.name":          "trans-http",
		"spec.template.spec.containers.0.ports.1.containerPort": 3000,
		//"spec.template.spec.containers.0.env.0.name":  "TEST_FFF",
		//"spec.template.spec.containers.0.env.0.value": "TEST_111",
		"spec.template.spec.containers.0.envFrom.0.secretRef.name": "db-credentials",
		"spec.template.spec.restartPolicy":                         "Always",
		"spec.template.spec.imagePullSecrets.0.name":               "gitlab-pull-secret",

		"params": map[string]any{
			"param1": "value1",
			"param2": "value2",
		},
	}

	s.service = map[string]any{
		"kind":                    "service",
		"$name":                   "translate",
		"selectorName":            "testSelector",
		"spec.type":               "NodePort",
		"spec.ports.0.name":       "trans-grpc",
		"spec.ports.0.port":       2021,
		"spec.ports.0.protocol":   "TCP",
		"spec.ports.0.targetPort": 3001,
		"spec.ports.1.name":       "trans-http",
		"spec.ports.1.port":       2020,
		"spec.ports.1.targetPort": 3000,

		"params": map[string]any{
			"param1": "value1",
			"param2": "value2",
		},
	}
}

func TestGeneratorSuiteSuite(t *testing.T) {
	suite.Run(t, new(GeneratorSuite))
}

func (s *GeneratorSuite) localGenerateDeploymentModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.deployment)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.deployment,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

func (s *GeneratorSuite) localGenerateServiceModel() models.GenerateDTO {
	bytes, _ := json.Marshal(s.service)

	kind := strings.ToLower(gjson.GetBytes(bytes, "kind").String())
	val := models.GenerateDTO{
		Kind:         kind,
		Meta:         s.service,
		CallbackURL:  s.callbackURL,
		Name:         gjson.GetBytes(bytes, "$name").String(),
		SelectorName: gjson.GetBytes(bytes, "selectorName").String(),
		Params: map[string]any{
			"params1": "value1",
			"params2": "value2",
		},
		Env:   gjson.GetBytes(bytes, "env").Map(),
		Image: gjson.GetBytes(bytes, "image").String(),
	}

	return val
}

// Testing generate deployment
func (s *GeneratorSuite) Test_GenerateDeployment() {
	model := s.localGenerateDeploymentModel()
	result, err := s.generator.Generate(model)

	require.NoError(s.T(), err)
	require.Equal(s.T(), result.Kind, models.DEPLOYMENT)

	portsName := gjson.Get(result.Config, "spec.template.spec.containers.0.ports.1.name")
	require.Equal(s.T(), portsName.String(), "trans-http")
	image := gjson.Get(result.Config, "spec.template.spec.containers.0.image")
	require.Equal(s.T(), image.String(), "imageTest")
}

// Testing generate service
func (s *GeneratorSuite) Test_GenerateService() {
	model := s.localGenerateServiceModel()
	result, err := s.generator.Generate(model)

	require.NoError(s.T(), err)
	require.Equal(s.T(), result.Kind, models.SERVICE)

	portsName := gjson.Get(result.Config, "spec.ports.0.name")
	require.Equal(s.T(), portsName.String(), "trans-grpc")
	portsValue := gjson.Get(result.Config, "spec.ports.0.port")
	require.Equal(s.T(), portsValue.String(), "2021")
	portTargetValue := gjson.Get(result.Config, "spec.ports.0.targetPort")
	require.Equal(s.T(), portTargetValue.String(), "3001")
	portsProtocol := gjson.Get(result.Config, "spec.ports.0.protocol")
	require.Equal(s.T(), portsProtocol.String(), "TCP")
}
