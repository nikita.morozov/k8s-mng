package generator

import (
	"encoding/json"
	"fmt"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	models2 "kuber-crud/models"
	"strings"
)

type generator struct {
	app          string
	callbackName string
	paramsName   string
}

type Generator interface {
	Generate(value models2.GenerateDTO) (models2.GenerateDTO, error)
	AddCustomValue(value models2.GenerateDTO) models2.GenerateDTO
}

func (g *generator) AddValue(item, path string, value any) (string, error) {
	return sjson.Set(item, path, value)
}

func (g *generator) commonGenerateDeployment(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	item, err := g.AddValue(value.Config, "apiVersion", "apps/v1")
	item, err = g.AddValue(item, "kind", value.Kind)

	item, err = g.AddValue(item, "spec.template.metadata.labels.tier", value.SelectorName)
	item, err = g.AddValue(item, "metadata.labels.app", g.app)
	item, err = g.AddValue(item, "spec.selector.matchLabels.app", g.app)
	item, err = g.AddValue(item, "spec.template.metadata.labels.app", g.app)
	item, err = g.AddValue(item, "spec.template.spec.restartPolicy", "Always")
	value.SetConfig(item)
	value, err = g.SetName(value)
	return value, err
}

func (g *generator) SetName(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	config, err := g.AddValue(value.Config, "metadata.name", value.Name)

	if value.Kind == models2.DEPLOYMENT {
		config, err = g.AddValue(config, "spec.template.spec.containers.0.name", value.Name)
	}

	value.SetConfig(config)
	return value, err
}

func (g *generator) commonGenerateService(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	item, err := g.AddValue(value.Config, "apiVersion", "v1")
	item, err = g.AddValue(item, "kind", value.Kind)
	item, err = g.AddValue(item, "metadata.labels.app", g.app)
	item, err = g.AddValue(item, "spec.selector.app", g.app)
	item, err = g.AddValue(item, "spec.selector.tier", value.SelectorName)
	value.SetConfig(item)
	value, err = g.SetName(value)
	return value, err
}
func (g *generator) commonGenerateConfigMap(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	item, err := g.AddValue(value.Config, "apiVersion", "v1")
	item, err = g.AddValue(item, "kind", value.Kind)
	item, err = g.AddValue(item, "metadata.labels", map[string]any{
		"io.kompose.service": value.SelectorName,
	})
	value.SetConfig(item)
	value, err = g.SetName(value)
	return value, err
}

func (g *generator) commonGeneratePersistentVolume(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	item, err := g.AddValue(value.Config, "apiVersion", "v1")
	item, err = g.AddValue(item, "kind", "PersistentVolume")
	item, err = g.AddValue(item, "metadata.labels.app", g.app)
	value.SetConfig(item)
	value, err = g.SetName(value)
	return value, err
}

func (g *generator) commonGeneratePersistentVolumeClaim(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	item, err := g.AddValue(value.Config, "apiVersion", "v1")
	item, err = g.AddValue(item, "kind", "PersistentVolume")
	item, err = g.AddValue(item, "metadata.labels.app", g.app)
	value.SetConfig(item)
	value, err = g.SetName(value)
	return value, err
}

func (g *generator) Generate(value models2.GenerateDTO) (models2.GenerateDTO, error) {
	var err error

	switch value.Kind {
	case models2.DEPLOYMENT:
		value, err = g.commonGenerateDeployment(value)
	case models2.SERVICE:
		value, err = g.commonGenerateService(value)
	case models2.CONFIGMAP:
		value, err = g.commonGenerateConfigMap(value)
	case models2.PERSISTENT_VOLUME:
		value, err = g.commonGeneratePersistentVolume(value)
	case models2.PERSISTENT_VOLUME_CLAIM:
		value, err = g.commonGeneratePersistentVolumeClaim(value)
	}

	return g.AddCustomValue(value), err
}

func (g *generator) needSkipPath(path string) bool {
	skipPath := []string{"params", "$name", "app", "selector", "image", "env"}
	for _, p := range skipPath {
		if strings.Contains(p, path) {
			return true
		}
	}

	return false
}

func (g *generator) AddEnv(value models2.GenerateDTO) models2.GenerateDTO {
	config := gjson.Get(value.Config, "spec.template.spec.containers")
	var r []any
	err := json.Unmarshal([]byte(config.String()), &r)
	if err != nil {
		return value
	}

	for index, _ := range r {
		for name, val := range value.Env {
			path := fmt.Sprintf("spec.template.spec.containers.%d.env.-1", index)

			conf, err := g.AddValue(value.Config, path, map[string]string{
				"name":  name,
				"value": val.String(),
			})

			if err != nil {
				continue
			}

			value.SetConfig(conf)
		}
	}

	return value
}

func (g *generator) AddCallback(value models2.GenerateDTO) models2.GenerateDTO {
	config := gjson.Get(value.Config, "spec.template.spec.containers")
	var r []any
	err := json.Unmarshal([]byte(config.String()), &r)
	if err != nil {
		return value
	}

	for index, _ := range r {
		path := fmt.Sprintf("spec.template.spec.containers.%d.env.-1", index)

		conf, err := g.AddValue(value.Config, path, map[string]string{
			"name":  g.callbackName,
			"value": value.CallbackURL,
		})

		if value.Params != nil {
			bytes, err := json.Marshal(value.Params)
			if err != nil {
				continue
			}
			conf, err = g.AddValue(conf, path, map[string]string{
				"name":  g.paramsName,
				"value": string(bytes),
			})
		}

		if err != nil {
			continue
		}
		value.SetConfig(conf)
	}

	return value
}

func (g *generator) AddCustomValue(value models2.GenerateDTO) models2.GenerateDTO {
	for path, val := range value.Meta {
		if path == "image" {
			v, err := g.AddValue(value.Config, "spec.template.spec.containers.0.image", val)
			if err == nil {
				value.SetConfig(v)
			}
		}
		if g.needSkipPath(path) {
			continue
		}

		r, err := g.AddValue(value.Config, path, val)
		if err == nil {
			value.SetConfig(r)
		}
	}

	if value.Kind == models2.DEPLOYMENT {
		value = g.AddEnv(value)
		value = g.AddCallback(value)
	}

	return value
}

func NewGenerator(app, callbackName, paramsName string) Generator {
	return &generator{
		app,
		callbackName,
		paramsName,
	}
}
