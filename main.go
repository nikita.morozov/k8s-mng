package main

import (
	"context"
	"github.com/go-redis/redis/v8"
	"github.com/labstack/echo/v4"
	"github.com/nats-io/nats.go"
	tp "gitlab.com/txt-dev-pub/treenity-protocol"
	"k8s.io/client-go/kubernetes"
	restclient "k8s.io/client-go/rest"
	"kuber-crud/delivery/http"
	"kuber-crud/delivery/treenity"
	generator2 "kuber-crud/generator"
	interactors2 "kuber-crud/interactors"
	"kuber-crud/repository"
	kuberRepo2 "kuber-crud/repository/kuberRepo"
	"kuber-crud/usecase"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"
)

func main() {
	redisAddress := os.Getenv("REDIS_SERVER")
	natsAddress := os.Getenv("NATS_SERVER")
	httpAddress := os.Getenv("HTTP_SERVER")
	natsConnectDuration := os.Getenv("NATS_CONNECT_DURATION")
	natsDuration := os.Getenv("NATS_SERVER_DURATION")
	redisDurationEnv := os.Getenv("REDIS_DURATION")
	urlProtocol := os.Getenv("URL_PROTOCOL")
	appName := os.Getenv("APP_NAME")
	callbackName := os.Getenv("CALLBACK_NAME")
	paramsName := os.Getenv("PARAMS_NAME")
	debug := os.Getenv("DEBUG")

	natsConnectDurationVal := time.Duration(1)
	if natsDuration != "" {
		val, err := strconv.Atoi(natsConnectDuration)
		if err == nil {
			natsConnectDurationVal = time.Duration(val)
		}
	}

	wait := make(chan bool)
	var nc *nats.Conn

	go func() {
		for {
			time.Sleep(natsConnectDurationVal * time.Millisecond)
			v, err := nats.Connect(natsAddress)
			if err == nil {
				nc = v
				wait <- true
				break
			}
		}
	}()

	<-wait

	duration := time.Duration(0)
	redisDuration := time.Duration(0)

	if natsDuration != "" {
		val, err := strconv.Atoi(natsDuration)
		if err == nil {
			duration = time.Duration(val)
		}
	}

	if redisDurationEnv != "" {
		val, err := strconv.Atoi(redisDurationEnv)
		if err == nil {
			redisDuration = time.Duration(val)
		}
	}

	server := tp.NewNatsServer(nc, duration*time.Millisecond, debug == "TRUE")
	serviceMgr := tp.NewServiceManager(server)

	config, err := restclient.InClusterConfig()
	if err != nil {
		panic(err)
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err)
	}

	redisOpts := &redis.Options{
		Addr:     redisAddress,
		DB:       0,
		Password: "",
	}

	generator := generator2.NewGenerator(appName, callbackName, paramsName)

	redisHandler := repository.NewRedisRepository(redis.NewClient(redisOpts), redisDuration)
	deployment := kuberRepo2.NewDeploymentRepository(client)
	service := kuberRepo2.NewServiceRepository(client)
	configMap := kuberRepo2.NewConfigMapRepository(client)
	persistentVolume := kuberRepo2.NewPersistentVolumesRepository(client)
	persistentVolumeClaim := kuberRepo2.NewPersistentVolumeClaimsRepository(client)

	interactorsDeployment := interactors2.NewDeploymentInteractor(deployment, redisHandler, generator)
	interactorsService := interactors2.NewServiceInteractor(service, redisHandler, generator)
	interactorsConfigMap := interactors2.NewConfigMapInteractor(configMap, redisHandler, generator)
	interactorsPresentationVolume := interactors2.NewPersistentVolumeInteractor(persistentVolume, redisHandler, generator)
	interactorsPresentationVolumeClaim := interactors2.NewPersistentVolumeClaimInteractor(persistentVolumeClaim, redisHandler, generator)

	kuberUC := usecase.NewKuberUsecase(interactorsDeployment, interactorsService, interactorsConfigMap, interactorsPresentationVolume, interactorsPresentationVolumeClaim, redisHandler)

	serviceKuber := treenity.NewKuberService(kuberUC)
	serviceMgr.Register(context.TODO(), urlProtocol, serviceKuber)

	e := echo.New()

	http.NewKuberHttpHandler(e, kuberUC)

	go func() {
		err = e.Start(httpAddress)
		if err != nil {
			log.Fatal(err)
		}
	}()

	ch := make(chan os.Signal, 1)
	signal.Notify(ch, os.Interrupt, syscall.SIGTERM)
	<-ch
}
