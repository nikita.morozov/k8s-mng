package http

import (
	"github.com/labstack/echo/v4"
	"kuber-crud/common"
	"kuber-crud/usecase"
	"net/http"
)

type KuberHttpHandler struct {
	uc usecase.KuberUsecase
}

func (h *KuberHttpHandler) Stop(c echo.Context) error {
	err := h.uc.Stop()
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func (h *KuberHttpHandler) Run(c echo.Context) error {
	err := h.uc.Run()
	if err != nil {
		return err
	}

	return c.NoContent(http.StatusOK)
}

func NewKuberHttpHandler(e *echo.Echo, uc usecase.KuberUsecase) {
	handler := KuberHttpHandler{
		uc: uc,
	}

	e.GET(common.API_VER_1_0+"/stop", handler.Stop)
	e.GET(common.API_VER_1_0+"/run", handler.Run)
}
