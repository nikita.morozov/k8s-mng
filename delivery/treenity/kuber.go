package treenity

import (
	"context"
	"github.com/mitchellh/mapstructure"
	core "gitlab.com/txt-dev-pub/treenity-core"
	tp "gitlab.com/txt-dev-pub/treenity-protocol"
	"kuber-crud/models"
	"kuber-crud/usecase"
)

type KuberTreeenityHandler struct {
	uc usecase.KuberUsecase
}

type TeDel struct {
	Value models.DeleteDTO `json:"value"`
}

func (h *KuberTreeenityHandler) Apply(ctx context.Context, in core.Data, out core.Pipe) {
	var msg models.RequestConfigs
	err := mapstructure.Decode(in, &msg)
	if err != nil {
		out <- core.NodeDataError(err)
		return
	}
	err = h.uc.Apply(msg.Items, msg.Params, msg.Group, msg.Method)
	if err != nil {
		out <- core.NodeDataError(err)
		return
	}

	out <- core.NodeDataComplete()
}

func (h *KuberTreeenityHandler) Delete(ctx context.Context, in core.Data, out core.Pipe) {
	var msg TeDel

	err := mapstructure.Decode(in, &msg)
	if err != nil {
		out <- core.NodeDataError(err)
		return
	}

	err = h.uc.DeleteGroup(msg.Value.Group, false)
	if err != nil {
		out <- core.NodeDataError(err)
		return
	}

	out <- core.NodeDataComplete()
}

func NewKuberService(uc usecase.KuberUsecase) core.Runnable {
	handler := KuberTreeenityHandler{
		uc: uc,
	}

	return core.CreateService(core.M{
		"apply":  tp.CreateMethod(true, true, handler.Apply, tp.NewSimpleWrapper),
		"delete": tp.CreateMethod(true, true, handler.Delete, tp.NewSimpleWrapper),
	}, nil)
}
