package repository

import (
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"github.com/go-redis/redismock/v8"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"kuber-crud/models"
	"testing"
	"time"
)

type RedisRepoSuite struct {
	suite.Suite
	client     *redis.Client
	mock       redismock.ClientMock
	duration   time.Duration
	prefix     string
	repository RedisRepo
}

func (s *RedisRepoSuite) BeforeTest(_, _ string) {
	client, mock := redismock.NewClientMock()
	s.duration = 0
	s.repository = NewRedisRepository(client, s.duration)
	s.client = client
	s.mock = mock
	s.prefix = "kubernetes"
}

func (s *RedisRepoSuite) AfterTest(_, _ string) {
	require.NoError(s.T(), s.mock.ExpectationsWereMet())
}

func TestRedisRepo(t *testing.T) {
	suite.Run(t, new(RedisRepoSuite))
}

func (s *RedisRepoSuite) getKey(group, tier, _type string) string {
	return fmt.Sprintf("%s:%s:%s:%s", s.prefix, group, _type, tier)
}

func (s *RedisRepoSuite) Test_redisCreate() {
	value := models.Node{
		Tier: "Transsss",
		Type: "Deployment",
	}

	group := "test-group"
	payload, err := json.Marshal(value)
	require.Nil(s.T(), err)
	require.NoError(s.T(), err)

	s.mock.ExpectSet(s.getKey(group, value.Tier, value.Type), payload, s.duration).SetVal("1")
	err = s.repository.Create(&value, "test-group")
	require.NoError(s.T(), err)
}

func (s *RedisRepoSuite) Test_redisUpdate() {
	value := models.Node{
		Tier: "Transsss",
		Type: "Deployment",
	}

	group := "test-group"
	payload, err := json.Marshal(value)
	require.Nil(s.T(), err)
	require.NoError(s.T(), err)

	s.mock.ExpectSet(s.getKey(group, value.Tier, value.Type), payload, s.duration).SetVal("1")
	err = s.repository.Update(&value, "test-group")
	require.NoError(s.T(), err)
}

func (s *RedisRepoSuite) Test_redisList() {
	value := models.Node{
		Tier: "Transsss",
		Type: "Deployment",
	}

	group := "test-group"

	payload, err := json.Marshal(value)
	require.Nil(s.T(), err)
	require.NoError(s.T(), err)

	s.mock.ExpectKeys(s.getKey(group, value.Tier, value.Type)).SetVal([]string{s.getKey(group, value.Tier, value.Type)})
	s.mock.ExpectGet(s.getKey(group, value.Tier, value.Type)).SetVal(string(payload))
	items, err := s.repository.List(group, value.Type, value.Tier)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), items)
	require.Equal(s.T(), len(*items), 1)
}

func (s *RedisRepoSuite) Test_redisGet() {
	value := models.Node{
		Tier: "Transsss",
		Type: "Deployment",
	}

	group := "test-group"

	payload, err := json.Marshal(value)
	require.Nil(s.T(), err)
	require.NoError(s.T(), err)

	s.mock.ExpectGet(s.getKey(group, value.Tier, value.Type)).SetVal(string(payload))
	item, err := s.repository.Get(group, value.Type, value.Tier)
	require.NoError(s.T(), err)
	require.NotNil(s.T(), item)
}

func (s *RedisRepoSuite) Test_redisRemove() {
	value := models.Node{
		Tier: "Transsss",
		Type: "Deployment",
	}

	group := "test-group"

	s.mock.ExpectDel(s.getKey(group, value.Tier, value.Type)).SetVal(1)
	err := s.repository.Remove(group, value.Type, value.Tier)
	require.NoError(s.T(), err)
}
