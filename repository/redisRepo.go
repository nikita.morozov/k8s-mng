package repository

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/go-redis/redis/v8"
	"kuber-crud/models"
	"time"
)

type RedisRepo interface {
	Create(value *models.Node, group string) error
	Update(value *models.Node, group string) error
	Remove(group, _type, tier string) error
	Get(group, _type, tier string) (*models.Node, error)
	List(group, _type, tier string) (*[]models.Node, error)
}

type redisRepo struct {
	client   *redis.Client
	ctx      context.Context
	prefix   string
	duration time.Duration
}

func (r *redisRepo) getKey(group, tier, _type string) string {
	return fmt.Sprintf("%s:%s:%s:%s", r.prefix, group, _type, tier)
}

func (r *redisRepo) Create(value *models.Node, group string) error {
	payload, err := json.Marshal(value)
	if err != nil {
		return err
	}

	return r.client.Set(r.ctx, r.getKey(group, value.Tier, value.Type), payload, r.duration).Err()
}

func (r *redisRepo) Update(value *models.Node, group string) error {
	return r.Create(value, group)
}

func (r *redisRepo) List(group, _type, tier string) (*[]models.Node, error) {
	res, err := r.client.Keys(r.ctx, r.getKey(group, tier, _type)).Result()
	if err != nil {
		return nil, err
	}

	values := make([]models.Node, 0)

	for _, key := range res {
		res := r.client.Get(r.ctx, key)
		if res.Err() != nil {
			return nil, res.Err()
		}

		data, err := res.Bytes()
		if err != nil {
			continue
		}
		var result models.Node
		err = json.Unmarshal(data, &result)
		if err != nil {
			return nil, err
		}

		values = append(values, result)
	}

	return &values, err
}

func (r *redisRepo) Get(group, _type, tier string) (*models.Node, error) {
	res := r.client.Get(r.ctx, r.getKey(group, tier, _type))
	if res.Err() != nil {
		return nil, res.Err()
	}

	data, err := res.Bytes()
	if err != nil {
		return nil, err
	}

	var result models.Node
	err = json.Unmarshal(data, &res)
	if err != nil {
		return nil, err
	}
	return &result, err

}

func (r *redisRepo) Remove(group, _type, tier string) error {
	return r.client.Del(r.ctx, r.getKey(group, tier, _type)).Err()
}

func NewRedisRepository(client *redis.Client, redisDuration time.Duration) RedisRepo {
	return &redisRepo{
		prefix:   "kubernetes",
		ctx:      context.Background(),
		client:   client,
		duration: redisDuration,
	}
}
