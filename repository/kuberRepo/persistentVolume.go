package kuberRepo

import (
	"context"
	"fmt"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
)

type PersistentVolumeRepo interface {
	Create(item *apiv1.PersistentVolume) (*apiv1.PersistentVolume, error)
	Update(item *apiv1.PersistentVolume) (*apiv1.PersistentVolume, error)
	Get(name string) (*apiv1.PersistentVolume, error)
	Delete(name string) error
}

type persistentVolumes struct {
	client corev1.CoreV1Interface
}

func (d persistentVolumes) Create(item *apiv1.PersistentVolume) (*apiv1.PersistentVolume, error) {
	fmt.Println("Creating persistentVolume...")
	result, err := d.client.PersistentVolumes().Create(context.TODO(), item, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Created persistentVolume %q.\n", result.GetName())
	return result, err
}
func (d persistentVolumes) Update(item *apiv1.PersistentVolume) (*apiv1.PersistentVolume, error) {
	fmt.Println("Updating persistentVolume...")
	result, err := d.client.PersistentVolumes().Update(context.TODO(), item, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Updated persistentVolume %q.\n", result.GetName())
	return result, err
}

func (d persistentVolumes) Get(name string) (*apiv1.PersistentVolume, error) {
	result, err := d.client.PersistentVolumes().Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return result, err
}

func (d persistentVolumes) Delete(name string) error {
	fmt.Printf("Deleteing persistentVolume...%q \n", name)
	return d.client.PersistentVolumes().Delete(context.TODO(), name, metav1.DeleteOptions{})
}

func NewPersistentVolumesRepository(handlerApi *kubernetes.Clientset) PersistentVolumeRepo {
	client := handlerApi.CoreV1()
	return &persistentVolumes{
		client: client,
	}
}
