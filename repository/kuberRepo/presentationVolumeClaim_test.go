package kuberRepo

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"testing"
)

type kuberPresentationVolumeClaimSuite struct {
	suite.Suite
	client             corev1.CoreV1Interface
	name               string
	presentationVolume string
	ctx                context.Context
}

func (s *kuberPresentationVolumeClaimSuite) BeforeTest(_, _ string) {
	client := fake.NewSimpleClientset()
	s.client = client.CoreV1()
	s.ctx = context.TODO()
	s.name = "default"
	s.presentationVolume = `{
  "apiVersion": "v1",
  "kind": "PersistentVolumeClaim",
  "metadata": {
    "name": "test",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "storageClassName": "hcloud-volumes",
    "accessModes": [
      "ReadWriteOnce"
    ],
    "resources": {
      "requests": {
        "storage": "50Gi"
      }
    }
  }
}
`
}

func TestkuberPresentationVolumeClaimRepo(t *testing.T) {
	suite.Run(t, new(kuberPresentationVolumeClaimSuite))
}
func (s *kuberPresentationVolumeClaimSuite) convertJsonToConfig(configmap string) (*apiv1.PersistentVolumeClaim, error) {
	var item *apiv1.PersistentVolumeClaim
	err := json.Unmarshal([]byte(configmap), &item)
	return item, err
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsCreate() {
	presentationVolume, err := s.convertJsonToConfig(s.presentationVolume)
	require.NoError(s.T(), err)

	res, err := s.client.PersistentVolumeClaims(s.name).Create(s.ctx, presentationVolume, metav1.CreateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsCreateWithError() {
	res, err := s.client.PersistentVolumeClaims(s.name).Create(s.ctx, nil, metav1.CreateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsUpdate() {
	presentationVolume, err := s.convertJsonToConfig(s.presentationVolume)
	require.NoError(s.T(), err)
	s.Test_kluberPersistentVolumeClaimsCreate()
	res, err := s.client.PersistentVolumeClaims(s.name).Update(s.ctx, presentationVolume, metav1.UpdateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsUpdateWithError() {
	presentationVolume, err := s.convertJsonToConfig(s.presentationVolume)
	require.NoError(s.T(), err)
	res, err := s.client.PersistentVolumeClaims(s.name).Update(s.ctx, presentationVolume, metav1.UpdateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsGet() {
	name := "test"
	s.Test_kluberPersistentVolumeClaimsCreate()
	serviceRes, err := s.client.PersistentVolumeClaims(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsGetWithError() {
	name := "test"
	res, err := s.client.PersistentVolumeClaims(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsDelete() {
	s.Test_kluberPersistentVolumeClaimsCreate()
	name := "test"
	err := s.client.PersistentVolumeClaims(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.NoError(s.T(), err)
}

func (s *kuberPresentationVolumeClaimSuite) Test_kluberPersistentVolumeClaimsDeleteWithError() {
	name := "test"
	err := s.client.PersistentVolumeClaims(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.Error(s.T(), err)
}
