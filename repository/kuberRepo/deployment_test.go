package kuberRepo

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	v1 "k8s.io/api/apps/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	appsv1 "k8s.io/client-go/kubernetes/typed/apps/v1"
	"testing"
)

type KuberDeploymentSuite struct {
	suite.Suite
	client     appsv1.AppsV1Interface
	name       string
	deployment string
	ctx        context.Context
}

func (s *KuberDeploymentSuite) BeforeTest(_, _ string) {
	client := fake.NewSimpleClientset()
	s.client = client.AppsV1()
	s.name = "default"
	s.ctx = context.TODO()
	s.deployment = `{
  "apiVersion": "apps/v1",
  "kind": "Deployment",
  "metadata": {
    "name": "transservice",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "selector": {
      "matchLabels": {
        "app": "crypto-fairy"
      }
    },
    "template": {
      "metadata": {
        "labels": {
          "app": "crypto-fairy",
          "tier": "TransService"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "transservice",
            "image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.15",
            "ports": [
              {
                "name": "trans-grpc",
                "containerPort": 3001
              },
              {
                "name": "trans-http",
                "containerPort": 3000
              }
            ],
            "env": [
              {
                "name": "HTTP_SERVER",
                "value": "0.0.0.0:3000"
              },
              {
                "name": "GRPC_SERVER",
                "value": "0.0.0.0:3001"
              },
              {
                "name": "AUTO_TRANSLATE",
                "value": "FALSE"
              }
            ],
            "envFrom": [
              {
                "secretRef": {
                  "name": "db-credentials"
                }
              }
            ]
          }
        ],
        "restartPolicy": "Always",
        "imagePullSecrets": [
          {
            "name": "gitlab-pull-secret"
          }
        ]
      }
    }
  }
}`
}

func TestKuberDeploymentRepo(t *testing.T) {
	suite.Run(t, new(KuberDeploymentSuite))
}
func (s *KuberDeploymentSuite) convertJsonToConfig(deployment string) (*v1.Deployment, error) {
	var item *v1.Deployment
	err := json.Unmarshal([]byte(deployment), &item)
	return item, err
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentCreate() {
	deployment, err := s.convertJsonToConfig(s.deployment)
	require.NoError(s.T(), err)

	res, err := s.client.Deployments(s.name).Create(s.ctx, deployment, metav1.CreateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentCreateWithError() {
	res, err := s.client.Deployments(s.name).Create(s.ctx, nil, metav1.CreateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentUpdate() {
	deployment, err := s.convertJsonToConfig(s.deployment)
	require.NoError(s.T(), err)
	s.Test_kuberDeploymentCreate()
	res, err := s.client.Deployments(s.name).Update(s.ctx, deployment, metav1.UpdateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentUpdateWithError() {
	deployment, err := s.convertJsonToConfig(s.deployment)
	require.NoError(s.T(), err)
	res, err := s.client.Deployments(s.name).Update(s.ctx, deployment, metav1.UpdateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentGet() {
	name := "transservice"
	s.Test_kuberDeploymentCreate()
	serviceRes, err := s.client.Deployments(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentGetWithError() {
	name := "transservice"
	res, err := s.client.Deployments(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentDelete() {
	s.Test_kuberDeploymentCreate()
	name := "transservice"
	err := s.client.Deployments(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.NoError(s.T(), err)
}

func (s *KuberDeploymentSuite) Test_kuberDeploymentDeleteWithError() {
	name := "transservice"
	err := s.client.Deployments(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.Error(s.T(), err)
}
