package kuberRepo

import (
	"context"
	"fmt"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
)

type ConfigMapRepo interface {
	Create(item *apiv1.ConfigMap) (*apiv1.ConfigMap, error)
	Update(item *apiv1.ConfigMap) (*apiv1.ConfigMap, error)
	Get(name string) (*apiv1.ConfigMap, error)
	Delete(name string) error
}

type configmapRepo struct {
	client corev1.CoreV1Interface
}

func (d configmapRepo) Create(item *apiv1.ConfigMap) (*apiv1.ConfigMap, error) {
	fmt.Println("Creating configmap...")
	result, err := d.client.ConfigMaps(apiv1.NamespaceDefault).Create(context.TODO(), item, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Created configmap %q.\n", result.GetName())
	return result, err
}
func (d configmapRepo) Update(item *apiv1.ConfigMap) (*apiv1.ConfigMap, error) {
	fmt.Println("Updating configmap...")
	result, err := d.client.ConfigMaps(apiv1.NamespaceDefault).Update(context.TODO(), item, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Updated configmap %q.\n", result.GetName())
	return result, err
}

func (d configmapRepo) Get(name string) (*apiv1.ConfigMap, error) {
	result, err := d.client.ConfigMaps(apiv1.NamespaceDefault).Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return result, err
}

func (d configmapRepo) Delete(name string) error {
	fmt.Printf("Deleteing configmap...%q \n", name)
	return d.client.ConfigMaps(apiv1.NamespaceDefault).Delete(context.TODO(), name, metav1.DeleteOptions{})
}

func NewConfigMapRepository(handlerApi *kubernetes.Clientset) ConfigMapRepo {
	client := handlerApi.CoreV1()
	return &configmapRepo{
		client: client,
	}
}
