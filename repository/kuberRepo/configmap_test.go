package kuberRepo

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"testing"
)

type kuberConfigMapSuite struct {
	suite.Suite
	client    corev1.CoreV1Interface
	name      string
	configmap string
	ctx       context.Context
}

func (s *kuberConfigMapSuite) BeforeTest(_, _ string) {
	client := fake.NewSimpleClientset()
	s.client = client.CoreV1()
	s.name = "default"
	s.ctx = context.TODO()
	s.configmap = `
{
  "apiVersion": "v1",
  "data": {
    "SOCKET_URL": "http://test:8080",
    "DOMAIN": "http://test"
  },
  "kind": "ConfigMap",
  "metadata": {
    "creationTimestamp": null,
    "labels": {
      "io.kompose.service": "test-env"
    },
    "name": "test-env"
  }
}
`
}

func TestkuberConfigMapRepo(t *testing.T) {
	suite.Run(t, new(kuberConfigMapSuite))
}
func (s *kuberConfigMapSuite) convertJsonToConfig(configmap string) (*apiv1.ConfigMap, error) {
	var item *apiv1.ConfigMap
	err := json.Unmarshal([]byte(configmap), &item)
	return item, err
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapCreate() {
	configmap, err := s.convertJsonToConfig(s.configmap)
	require.NoError(s.T(), err)

	res, err := s.client.ConfigMaps(s.name).Create(s.ctx, configmap, metav1.CreateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapCreateWithError() {
	res, err := s.client.ConfigMaps(s.name).Create(s.ctx, nil, metav1.CreateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapUpdate() {
	configmap, err := s.convertJsonToConfig(s.configmap)
	require.NoError(s.T(), err)
	s.Test_kuberConfigmapCreate()
	res, err := s.client.ConfigMaps(s.name).Update(s.ctx, configmap, metav1.UpdateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapUpdateWithError() {
	configmap, err := s.convertJsonToConfig(s.configmap)
	require.NoError(s.T(), err)
	res, err := s.client.ConfigMaps(s.name).Update(s.ctx, configmap, metav1.UpdateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapGet() {
	name := "test-env"
	s.Test_kuberConfigmapCreate()
	serviceRes, err := s.client.ConfigMaps(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapGetWithError() {
	name := "test-env"
	res, err := s.client.ConfigMaps(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapDelete() {
	s.Test_kuberConfigmapCreate()
	name := "test-env"
	err := s.client.ConfigMaps(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.NoError(s.T(), err)
}

func (s *kuberConfigMapSuite) Test_kuberConfigmapDeleteWithError() {
	name := "test-env"
	err := s.client.ConfigMaps(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.Error(s.T(), err)
}
