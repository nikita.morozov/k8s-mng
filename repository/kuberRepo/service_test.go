package kuberRepo

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"testing"
)

type KuberServiceSuite struct {
	suite.Suite
	client  corev1.CoreV1Interface
	name    string
	service string
	ctx     context.Context
}

func (s *KuberServiceSuite) BeforeTest(_, _ string) {
	client := fake.NewSimpleClientset()
	s.client = client.CoreV1()
	s.name = "default"
	s.ctx = context.TODO()
	s.service = `{
  "apiVersion": "v1",
  "kind": "Service",
  "metadata": {
    "name": "translate",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "type": "NodePort",
    "selector": {
      "app": "crypto-fairy",
      "tier": "TransService"
    },
    "ports": [
      {
        "name": "trans-grpc",
        "port": 2021,
        "protocol": "TCP",
        "targetPort": 3001
      },
      {
        "name": "trans-http",
        "port": 2020,
        "targetPort": 3000
      }
    ]
  }
}`
}

func TestKuberServiceRepo(t *testing.T) {
	suite.Run(t, new(KuberServiceSuite))
}

func (s *KuberServiceSuite) convertJsonToConfig(service string) (*apiv1.Service, error) {
	var item *apiv1.Service
	err := json.Unmarshal([]byte(service), &item)
	return item, err
}

func (s *KuberServiceSuite) Test_kuberServiceCreate() {
	service, err := s.convertJsonToConfig(s.service)
	require.NoError(s.T(), err)

	serviceRes, err := s.client.Services(s.name).Create(s.ctx, service, metav1.CreateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *KuberServiceSuite) Test_kuberServiceCreateWithError() {
	serviceRes, err := s.client.Services(s.name).Create(s.ctx, nil, metav1.CreateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), serviceRes)
}

func (s *KuberServiceSuite) Test_kuberServiceUpdate() {
	service, err := s.convertJsonToConfig(s.service)
	require.NoError(s.T(), err)
	s.Test_kuberServiceCreate()
	serviceRes, err := s.client.Services(s.name).Update(s.ctx, service, metav1.UpdateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *KuberServiceSuite) Test_kuberServiceUpdateWithError() {
	service, err := s.convertJsonToConfig(s.service)
	require.NoError(s.T(), err)
	serviceRes, err := s.client.Services(s.name).Update(s.ctx, service, metav1.UpdateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), serviceRes)
}

func (s *KuberServiceSuite) Test_kuberServiceGet() {
	name := "translate"
	s.Test_kuberServiceCreate()
	serviceRes, err := s.client.Services(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *KuberServiceSuite) Test_kuberServiceGetWithError() {
	name := "translate"
	serviceRes, err := s.client.Services(s.name).Get(s.ctx, name, metav1.GetOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), serviceRes)
}

func (s *KuberServiceSuite) Test_kuberServiceDelete() {
	s.Test_kuberServiceCreate()
	name := "translate"
	err := s.client.Services(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.NoError(s.T(), err)
}

func (s *KuberServiceSuite) Test_kuberServiceDeleteWithError() {
	name := "translate"
	err := s.client.Services(s.name).Delete(s.ctx, name, metav1.DeleteOptions{})
	require.Error(s.T(), err)
}
