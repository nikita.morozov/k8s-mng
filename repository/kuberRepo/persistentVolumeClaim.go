package kuberRepo

import (
	"context"
	"fmt"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
)

type PersistentVolumeClaimRepo interface {
	Create(item *apiv1.PersistentVolumeClaim) (*apiv1.PersistentVolumeClaim, error)
	Update(item *apiv1.PersistentVolumeClaim) (*apiv1.PersistentVolumeClaim, error)
	Get(name string) (*apiv1.PersistentVolumeClaim, error)
	Delete(name string) error
}

type persistentVolumeClaim struct {
	client corev1.CoreV1Interface
}

func (d persistentVolumeClaim) Create(item *apiv1.PersistentVolumeClaim) (*apiv1.PersistentVolumeClaim, error) {
	fmt.Println("Creating persistentVolumeClaim...")
	result, err := d.client.PersistentVolumeClaims(apiv1.NamespaceDefault).Create(context.TODO(), item, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Created persistentVolumeClaim %q.\n", result.GetName())
	return result, err
}
func (d persistentVolumeClaim) Update(item *apiv1.PersistentVolumeClaim) (*apiv1.PersistentVolumeClaim, error) {
	fmt.Println("Updating persistentVolumeClaim...")
	result, err := d.client.PersistentVolumeClaims(apiv1.NamespaceDefault).Update(context.TODO(), item, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Updated persistentVolumeClaim %q.\n", result.GetName())
	return result, err
}

func (d persistentVolumeClaim) Get(name string) (*apiv1.PersistentVolumeClaim, error) {
	result, err := d.client.PersistentVolumeClaims(apiv1.NamespaceDefault).Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return result, err
}

func (d persistentVolumeClaim) Delete(name string) error {
	fmt.Printf("Deleteing persistentVolumeClaim...%q \n", name)
	return d.client.PersistentVolumeClaims(apiv1.NamespaceDefault).Delete(context.TODO(), name, metav1.DeleteOptions{})
}

func NewPersistentVolumeClaimsRepository(handlerApi *kubernetes.Clientset) PersistentVolumeClaimRepo {
	client := handlerApi.CoreV1()
	return &persistentVolumeClaim{
		client: client,
	}
}
