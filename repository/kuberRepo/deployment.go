package kuberRepo

import (
	"context"
	"fmt"
	v1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	appsv1 "k8s.io/client-go/kubernetes/typed/apps/v1"
)

type DeploymentRepo interface {
	Create(item *v1.Deployment) (*v1.Deployment, error)
	Update(item *v1.Deployment) (*v1.Deployment, error)
	Get(name string) (*v1.Deployment, error)
	Delete(name string) error
}

type deploymentRepo struct {
	client appsv1.AppsV1Interface
}

func (d deploymentRepo) Create(item *v1.Deployment) (*v1.Deployment, error) {
	fmt.Println("Creating deployment...")
	result, err := d.client.Deployments(apiv1.NamespaceDefault).Create(context.TODO(), item, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Created deployment %q.\n", result.GetName())
	return result, err
}
func (d deploymentRepo) Update(item *v1.Deployment) (*v1.Deployment, error) {
	fmt.Println("Updating deployment...")
	result, err := d.client.Deployments(apiv1.NamespaceDefault).Update(context.TODO(), item, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Updated deployment %q.\n", result.GetName())
	return result, err
}

func (d deploymentRepo) Get(name string) (*v1.Deployment, error) {
	result, err := d.client.Deployments(apiv1.NamespaceDefault).Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return result, err
}

func (d deploymentRepo) Delete(name string) error {
	fmt.Printf("Deleteing deployment...%q \n", name)
	return d.client.Deployments(apiv1.NamespaceDefault).Delete(context.TODO(), name, metav1.DeleteOptions{})
}

func NewDeploymentRepository(handlerApi *kubernetes.Clientset) DeploymentRepo {
	client := handlerApi.AppsV1()
	return &deploymentRepo{
		client: client,
	}
}
