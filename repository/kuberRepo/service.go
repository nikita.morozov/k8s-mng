package kuberRepo

import (
	"context"
	"fmt"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
)

type ServiceRepo interface {
	Create(item *apiv1.Service) (*apiv1.Service, error)
	Update(item *apiv1.Service) (*apiv1.Service, error)
	Get(name string) (*apiv1.Service, error)
	Delete(name string) error
}

type serviceRepo struct {
	client corev1.CoreV1Interface
}

func (s serviceRepo) Create(item *apiv1.Service) (*apiv1.Service, error) {
	fmt.Println("Creating service...")
	result, err := s.client.Services(apiv1.NamespaceDefault).Create(context.TODO(), item, metav1.CreateOptions{})
	if err != nil {
		return nil, err
	}

	fmt.Printf("Created service %q.\n", result.GetName())
	return result, err
}

func (s serviceRepo) Update(item *apiv1.Service) (*apiv1.Service, error) {
	fmt.Println("Updating service...")
	result, err := s.client.Services(apiv1.NamespaceDefault).Update(context.TODO(), item, metav1.UpdateOptions{})
	if err != nil {
		return nil, err
	}
	fmt.Printf("Updated service %q.\n", result.GetName())
	return result, err
}

func (s serviceRepo) Get(name string) (*apiv1.Service, error) {
	fmt.Println("Updating service...")
	result, err := s.client.Services(apiv1.NamespaceDefault).Get(context.TODO(), name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	fmt.Printf("Updated service %q.\n", result.GetName())
	return result, err
}

func (s serviceRepo) Delete(name string) error {
	return s.client.Services(apiv1.NamespaceDefault).Delete(context.TODO(), name, metav1.DeleteOptions{})
}

func NewServiceRepository(handlerApi *kubernetes.Clientset) ServiceRepo {
	client := handlerApi.CoreV1()
	return &serviceRepo{
		client: client,
	}
}
