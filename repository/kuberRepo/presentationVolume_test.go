package kuberRepo

import (
	"context"
	"encoding/json"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	"testing"
)

type kuberPersistentVolumeSuite struct {
	suite.Suite
	client             corev1.CoreV1Interface
	name               string
	presentationVolume string
	ctx                context.Context
}

func (s *kuberPersistentVolumeSuite) BeforeTest(_, _ string) {
	client := fake.NewSimpleClientset()
	s.client = client.CoreV1()
	s.ctx = context.TODO()
	s.presentationVolume = `{
  "kind": "PersistentVolume",
  "apiVersion": "v1",
  "metadata": {
    "name": "test",
    "labels": {
      "app": "crypto-fairy"
    }
  },
  "spec": {
    "capacity": {
      "storage": "2Gi"
    },
    "accessModes": [
      "ReadWriteOnce"
    ],
    "persistentVolumeReclaimPolicy": "Retain",
    "storageClassName": "fast",
    "gcePersistentDisk": {
      "pdName": 1
    }
  }
}
`
}

func TestkuberPersistentVolumeRepo(t *testing.T) {
	suite.Run(t, new(kuberPersistentVolumeSuite))
}
func (s *kuberPersistentVolumeSuite) convertJsonToConfig(configmap string) (*apiv1.PersistentVolume, error) {
	var item *apiv1.PersistentVolume
	err := json.Unmarshal([]byte(configmap), &item)
	return item, err
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeCreate() {
	presentationVolume, err := s.convertJsonToConfig(s.presentationVolume)
	require.NoError(s.T(), err)

	res, err := s.client.PersistentVolumes().Create(s.ctx, presentationVolume, metav1.CreateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeCreateWithError() {
	res, err := s.client.PersistentVolumes().Create(s.ctx, nil, metav1.CreateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeUpdate() {
	presentationVolume, err := s.convertJsonToConfig(s.presentationVolume)
	require.NoError(s.T(), err)
	s.Test_kuberPresentationVolumeCreate()
	res, err := s.client.PersistentVolumes().Update(s.ctx, presentationVolume, metav1.UpdateOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), res)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeUpdateWithError() {
	presentationVolume, err := s.convertJsonToConfig(s.presentationVolume)
	require.NoError(s.T(), err)
	res, err := s.client.PersistentVolumes().Update(s.ctx, presentationVolume, metav1.UpdateOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeGet() {
	name := "test"
	s.Test_kuberPresentationVolumeCreate()
	serviceRes, err := s.client.PersistentVolumes().Get(s.ctx, name, metav1.GetOptions{})
	require.NoError(s.T(), err)
	require.NotNil(s.T(), serviceRes)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeGetWithError() {
	name := "test"
	res, err := s.client.PersistentVolumes().Get(s.ctx, name, metav1.GetOptions{})
	require.Error(s.T(), err)
	require.Nil(s.T(), res)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeDelete() {
	s.Test_kuberPresentationVolumeCreate()
	name := "test"
	err := s.client.PersistentVolumes().Delete(s.ctx, name, metav1.DeleteOptions{})
	require.NoError(s.T(), err)
}

func (s *kuberPersistentVolumeSuite) Test_kuberPresentationVolumeDeleteWithError() {
	name := "test"
	err := s.client.PersistentVolumes().Delete(s.ctx, name, metav1.DeleteOptions{})
	require.Error(s.T(), err)
}
