<h1 align="center">
<img src="https://readme-typing-svg.demolab.com?font=Fira+Code&size=30&pause=5000&color=54F71A&background=FF4D1600&center=true&vCenter=false&multiline=true&width=435&height=49&lines=k8s" alt="Typing SVG" />
</h1>

<h2 align="center">
Module 
</h2>
<h2 align="center">
<img src="https://readme-typing-svg.demolab.com?font=Fira+Code&size=28&pause=5000&color=F78967&background=FF4D1600&center=true&vCenter=true&multiline=true&width=435&height=49&lines=Manager" alt="Typing SVG" />
</h2>

## Reception

- Manage deployment
- Manage service
- Manage configmap
- Manage volume
- Manage volumeClaim

## Methods

- Apply - params RequestConfigs, this method use for create/update config


- request example
- 
```go
pvc := map[string]any{
"kind":                               "PersistentVolumeClaim",
"$name":                              "transservice",
"selectorName":                       "testSelector",
"spec.capacity.storage":              "1Gi",
"spec.accessModes.0":                 "ReadWriteOnce",
"spec.persistentVolumeReclaimPolicy": "Retain",
"spec.storageClassName":              "fast",
"spec.gcePersistentDisk.pdName":      "pd-ssd-disk-1",
}

   pv := map[string]any{
       "kind":                               "PersistentVolume",
       "$name":                              "transservice",
       "selectorName":                       "testSelector",
       "spec.capacity.storage":              "1Gi",
       "spec.accessModes.0":                 "ReadWriteOnce",
       "spec.persistentVolumeReclaimPolicy": "Retain",
       "spec.storageClassName":              "fast",
       "spec.gcePersistentDisk.pdName":      "pd-ssd-disk-1",
   }

   env := map[string]any{
       "kind":             "configmap",
       "$name":            "transservice",
       "selectorName":     "testSelector",
       "data.SOCKET_URL1": "SOCKET_URL1",
   }

   pod := map[string]any{
       "kind":         "deployment",
       "$name":        "transservice",
       "selectorName": "testSelector",
       "env": map[string]any{
           "FieldENV3": "ValueFieldEnv",
       },
       //"spec.template.spec.containers.0.image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.14",
       "image": "registry.gitlab.com/aquix/go-translate-ms:v2.0.15",
       "spec.template.spec.containers.0.ports.0.name":          "trans-grpc",
       "spec.template.spec.containers.0.ports.0.containerPort": 3001,
       "spec.template.spec.containers.0.ports.1.name":          "trans-http",
       "spec.template.spec.containers.0.ports.1.containerPort": 3000,
       //"spec.template.spec.containers.0.env.0.name":  "TEST_FFF",
       //"spec.template.spec.containers.0.env.0.value": "TEST_111",
       "spec.template.spec.containers.0.envFrom.0.secretRef.name": "db-credentials",
       "spec.template.spec.restartPolicy":                         "Always",
       "spec.template.spec.imagePullSecrets.0.name":               "gitlab-pull-secret",

       "params": map[string]any{
           "param1": "value1",
           "param2": "value2",
       },
   }

   service := map[string]any{
       "kind":                    "service",
       "$name":                   "translate",
       "selectorName":            "testSelector",
       "spec.type":               "NodePort",
       "spec.ports.0.name":       "trans-grpc",
       "spec.ports.0.port":       2021,
       "spec.ports.0.protocol":   "TCP",
       "spec.ports.0.targetPort": 3001,
       "spec.ports.1.name":       "trans-http",
       "spec.ports.1.port":       2020,
       "spec.ports.1.targetPort": 3000,

       "params": map[string]any{
           "param1": "value1",
           "param2": "value2",
       },
   }

   deployment := map[string]any{
       "group": "transs",
       "items": []map[string]any{
           pod,
           pvc,
           pv,
           env,
           service,
       },
   }
```

```yaml
lifecycle:
            postStart:
              exec:
                command:
                  - "sh"
                  - "-c"
                  - >
                    sleep 5 &&
                    curl -XGET http://localhost:3000/api/v1/run
            preStop:
              exec:
                command:
                  - "sh"
                  - "-c"
                  - >
                    curl -XGET http://localhost:3000/api/v1/stop
```
=======
- REDIS_SERVER = use for save config in redis, and when use delete method, get config with name group and deleting from k8s (EXAMPLE: 0.0.0.0:1234)
- NATS_ADDRESS = use for connect nats for job with protocol (EXAMPLE: 0.0.0.0:1234)
- NATS_SERVER_DURATION = use for timeout connect server (EXAMPLE: 8600)
- REDIS_DURATION = use for timeout save row in redis DB (EXAMPLE: 8600)

## Kuber setup
```bash
kubectl create role pod-creator --verb=create --verb=get --verb=list --verb=update --verb=delete --resource=pods,services,deployments
kubectl create rolebinding default-pod-creator --role=pod-creator --serviceaccount=default:default --namespace=default
```
